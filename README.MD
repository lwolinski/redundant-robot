Intro
=======

This is a library for simulation and control of redundant robots.

This project uses the [Eigen library][eigen].

[eigen]: http://eigen.tuxfamily.org/index.php?title=Main_Page

## KinematicModel

The KinematicModel class allows for solving the inverse kinematics problem of any serial redundant manipulator. The user has to provide a subclass which will implement the methods for computing the forward kinematics, Jacobian matrix, end effector position and/or orientation error. The KinematicModelMDH is a subclass which does that using the modified Denavit-Hartenberg parameters.

## DynamicModel

The DynamicModel class allows for creating a dynamic model of the manipulator. The user has to provide a subclass which will implement a method for computing transformation matrices needed for deriving the dynamic model. The DynamicModelMDH is a subclass which does that using the modified Denavit-Hartenberg parameters.

## RobotModel

The RobotModel class allows for creating a (kinematic and dynamic) model of the serial redundant manipulator. It uses the KinematicModelMDH and DynamicModelMDH classes by creating one object of each class as its member. The user can replace them with their own classes as needed, see the KinematicModel and DynamicModel above.

Currently, the model can be used to solve the forward kinematics problem for a given joint variables, solve the inverse kinematics problem for a given end effector trajectory, solve the inverse dynamics problem for a given trajectory in the joint space.

## Trajectory

The Trajectory class is used to generate trajectory consisting of linear segments. The object of this class is created in the RobotModel::SolveInverseKinematics to compute the desired end effector position and velocity for a given moment.

Example of installation
=======

rm -rf build && mkdir build

cd build

cmake .. && make

Example of usage
=======

cd ../examples/app

rm -rf build && mkdir build

rm -rf results && mkdir results

cd build

cmake .. && make

./MyApp
