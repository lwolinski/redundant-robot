//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#ifndef KINEMATICSOLVERDATA_H_
#define KINEMATICSOLVERDATA_H_

#include <Eigen/Dense>

namespace RedundantRobot
{

enum pinvType {JP, JD, SVF, SVFmod, JT};
enum subtaskType {JointLimitGradient, JointLimit, ObstacleAvoidance, MultipleSubtasks, None};
enum multitaskType {Standard, SingularityRobust};
enum integrationType {EE, EM, ET, RK4};

struct KinematicSolverData
{
	double ErrorGain{0.0};
	double ErrorSaturation{0.01};
	pinvType HowToInvertJacobian{JP};
	double SingularValueTreshold{0.001};
	double DampingFactorMaxSquared{0.04};
	double AssignedSmallestSingularValue{0.005};
	double SVFshapeFactor{10.0};
	double SubtaskConstraintGain{0.02};
	subtaskType Subtask{None};
	multitaskType MultitaskType{Standard};
	Eigen::VectorXd JointLimitMax;
	Eigen::VectorXd JointLimitMin;
	Eigen::VectorXd JointMid;
	Eigen::MatrixXd ObstaclePoints{Eigen::MatrixXd::Zero(3,1)};
	double ObstacleSphereOfInfluence{0.5};
	double ObstacleUnityGain{0.3};
	double ObstacleAvoidanceNominalVel{0.5};
	integrationType IntegrationMethod{EE};
};

} //namespace RedundantRobot

#endif /* KINEMATICSOLVERDATA_H_ */
