//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#ifndef DYNAMICMODELMDH_H_
#define DYNAMICMODELMDH_H_

#include "DynamicModel.h"

namespace RedundantRobot
{

class DynamicModelMDH : public DynamicModel
{
public:
	DynamicModelMDH( int DOF, const Eigen::MatrixXd &AllRotAxes, const Eigen::VectorXd &Masses, const Eigen::MatrixXd &AllFirstMomentsOfMass, const Eigen::MatrixXd &AllInertias, const Eigen::Vector3d &Gravity, const Eigen::MatrixXd &MDHangles, const Eigen::VectorXd &MDHlengthsX, const Eigen::VectorXd &MDHlengthsZ );
	
	DynamicModelMDH( const DynamicModelMDH &other );
	
	DynamicModelMDH& operator= ( const DynamicModelMDH &other );
	
	virtual ~DynamicModelMDH();
	
	virtual Eigen::MatrixXd TransformationMatrix( const Eigen::VectorXd &JointPos, int j );

protected:
	Eigen::VectorXd MDHangles;
	Eigen::VectorXd MDHlengthsX;
	Eigen::VectorXd MDHlengthsZ;
};

} //namespace RedundantRobot

#endif /* DYNAMICMODELMDH_H_ */
