//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#ifndef TRAJECTORY_H_
#define TRAJECTORY_H_

#include <iostream>
#include <Eigen/Dense>
#include "KinematicSolverData.h"

namespace RedundantRobot
{

class Trajectory
{
	public:
		Trajectory( const Eigen::MatrixXd& Points, const Eigen::RowVectorXd& PointTimes, double AccelerationPeriod, double DecelerationPeriod, double BlendingPeriod );
		
		//~ Get the end effector position and velocity
		Eigen::VectorXd GetTrajectory(double t);
		
		Eigen::VectorXd GetStandardTrajectory(double t);
		
		Eigen::VectorXd GetBlendedTrajectory(double t);
		
		//~ Get the displacement along the path and the path speed when the velocity profile is parabolic
		Eigen::Vector2d ParabolicVelProfile( double SegmentLength, double LocalTime, double SegmentEndTime );
		
		//~ Get the displacement along the path and the path speed when the velocity profile is "sinusoidal"
		Eigen::Vector2d SinusoidalVelProfile( double SegmentLength, double LocalTime, double SegmentEndTime );
		
	protected:
		Eigen::MatrixXd Points; // 3*NumberOfPoints matrix; (x,y,z) coordinates of the points making up the path (linear segments)
		Eigen::RowVectorXd SegmentTimes; // NumberOfPoints row vector with times needed to complete each segment; the index corresponds to the point being the end of the segment (index - 1 corresponds to the point starting the segment)
		double AccelerationPeriod; // part of the segment time allocated for acceleration
		double DecelerationPeriod; // part of the segment time allocated for deceleration
		int SegmentNumber; // number of the current trajectory segment
		double PathTime; // time needed to complete the path from the start of the first segment to the end of the current segment
		double BlendingPeriod; // the fraction of the segment's time to blend two linear segments of the trajectory
};

} //namespace RedundantRobot

#endif /* TRAJECTORY_H_ */
