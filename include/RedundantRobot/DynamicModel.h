//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#ifndef DYNAMICMODEL_H_
#define DYNAMICMODEL_H_

#include <iostream>
#include <Eigen/Dense>

namespace RedundantRobot
{
	
class DynamicModel
{	
public:
	DynamicModel( int DOF, const Eigen::MatrixXd &AllRotAxes, const Eigen::VectorXd &Masses, const Eigen::MatrixXd &AllFirstMomentsOfMass, const Eigen::MatrixXd &AllInertias, const Eigen::Vector3d &Gravity );
	
	DynamicModel( const DynamicModel &other );
	
	DynamicModel& operator= ( const DynamicModel &other );
	
	virtual ~DynamicModel();
	
	virtual Eigen::MatrixXd TransformationMatrix( const Eigen::VectorXd &JointPos, int j ) = 0;
	
	Eigen::MatrixXd DynamicMatrices( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &JointVel );
	
	Eigen::VectorXd InverseDynamicsStep( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &JointVel, const Eigen::VectorXd &JointAcc );
	
	Eigen::Matrix3d skew( const Eigen::Vector3d &Vector );

protected:
	int DOF; // number of degrees of freedom
	Eigen::MatrixXd AllRotAxes;
	Eigen::VectorXd Masses;
	Eigen::MatrixXd AllFirstMomentsOfMass;
	Eigen::MatrixXd AllInertias;
	Eigen::Vector3d Gravity;
};

} //namespace RedundantRobot

#endif /* DYNAMICMODEL_H_ */
