//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#ifndef ROBOTMODEL_H_
#define ROBOTMODEL_H_

#include "KinematicModelMDH.h"
#include "DynamicModelMDH.h"

const double pi = M_PI;

//~ TODO: this class should have a member function to set some options (like step size, type of the solution -- what to do with the robot's redundancy, etc.) and to pass them to the kinematic and dynamic model. This parameters probably should be members to avoid passing too many things as function arguments.

//~ using RedundantRobot::KinematicModelMDH;
//~ using RedundantRobot::DynamicModelMDH;

namespace RedundantRobot
{

class RobotModel
{
public:
	//~ Create a model of LWR 4+ robot
	RobotModel();
	RobotModel( const Eigen::Vector3d &angles_EE, const Eigen::Vector3d &r_EE );
	
	//~ Create a model of any robot with given inertial and MDH parameters
	RobotModel( int DOF, const Eigen::VectorXd &MDHangles, const Eigen::VectorXd &MDHlengthsX, const Eigen::VectorXd &MDHlengthsZ, const Eigen::Vector3d &angles_EE, const Eigen::Vector3d &r_EE, const Eigen::MatrixXd &AllRotAxes, const Eigen::VectorXd &Masses, const Eigen::MatrixXd &AllFirstMomentsOfMass, const Eigen::MatrixXd &AllInertias, const Eigen::Vector3d &Gravity );
	
	RobotModel( const RobotModel &other );
	
	RobotModel& operator= ( const RobotModel &other );
	
	~RobotModel();

	//~ Solve the forward kinematics problem for given joint positions
	Eigen::MatrixXd ForwardKinematics( const Eigen::VectorXd &JointPos );
	
	//~ Compute the manipulator jacobian
	Eigen::MatrixXd GetJacobian( const Eigen::VectorXd &JointPos );
	
	//~ Solve the inverse kinematics problem for a given time period and return joint positions, velocities and accelerations
	Eigen::MatrixXd SolveInverseKinematics( const Eigen::VectorXd &InitialJointPos, const Eigen::VectorXd &InitialJointVel, const Eigen::MatrixXd& Points, const Eigen::RowVectorXd& SegmentTimes, double StartTime, double EndTime, double StepSize );
	//~ Overloaded version accepting the obstacles' locations
	Eigen::MatrixXd SolveInverseKinematics( const Eigen::VectorXd &InitialJointPos, const Eigen::VectorXd &InitialJointVel, const Eigen::MatrixXd& Points, const Eigen::RowVectorXd& SegmentTimes, double StartTime, double EndTime, double StepSize, const Eigen::MatrixXd &ObstacleLocations );
	
	//~ Assign a new tool (a new end effector frame)
	void SetNewTool( const Eigen::Vector3d &New_angles_EE, const Eigen::Vector3d &New_r_EE );
	
	//~ Solve the inverse dynamics problem for given joint positions, velocities and accelerations
	Eigen::MatrixXd SolveInverseDynamics( const Eigen::MatrixXd &JointPosVelAccTotal );
	
	//~ Allows to manage the parameters used in pseudoinversion of the Jacobian matrix
	void SetPseudoInverse( pinvType HowToInvertJacobian, double FirstValue = 0.0, double SecondValue = 0.0 );
	
	//~ Set the error gain for the CLIK method
	void SetErrorGain( double ErrorGain );
	
	//~ Set the saturation level of error for the CLIK method
	void SetErrorSaturation( double ErrorSaturation );
	
	//~ Set the integration method
	void SetIntegrationMethod( integrationType IntegrationMethod );
	
	//~ Set the secondary task in the redundancy resolution
	void SetSubtask( subtaskType Subtask );
	
	//~ Set the value of the subtask constraint gain (it scales the nullspace velocity)
	void SetSubtaskConstraintGain( double SubtaskConstraintGain );
	
	//~ Set the value of the subtask constraint gain (it scales the nullspace velocity), and the number of steps until it reaches the full value
	void SetSubtaskConstraintGainAndSteps( double SubtaskConstraintGain, int NewStepsUntilSubtask );
	
	//~ Set the type of multitask formulation
	void SetMultitaskType( multitaskType MultitaskType );
	
	//~ Set the joint limits
	void SetJointLimits( const Eigen::VectorXd &JointLimitMax, const Eigen::VectorXd &JointLimitMin );
	
	//~ Set the obstacles' locations
	void SetObstaclePoints( const Eigen::MatrixXd ObstacleLocations );
	
	//~ Set the parameters of the obstacle avoidance subtask
	void SetObstacleAvoidanceParameters( double ObstacleSphereOfInfluence, double ObstacleUnityGain, double ObstacleAvoidanceNominalVel );
	
	//~ Declare how much of the segment time is allocated for acceleration and deceleration. Set the fraction of the segment's time to blend two linear segments of the trajectory
	void SetAccelerationDecelerationAndBlendingPeriod( double NewAccelerationPeriod, double NewDecelerationPeriod, double NewBlendingPeriod = 0.0 );
	
	//~ Declare how much of the segment time is allocated for acceleration and deceleration
	void SetAccelerationAndDecelerationPeriod( double NewAccelerationPeriod, double NewDecelerationPeriod );
	
private:
	int DOF; // number of degrees of freedom
	KinematicModelMDH *KinematicRobotModel;
	DynamicModelMDH *DynamicRobotModel;
	
	int StepsUntilSubtask{20}; // number of steps until the subtask constraint runs with the full gain
	
	//~ Parameters of the trajectory generator:
	double AccelerationPeriod{0.5}; // part of the segment time allocated for acceleration
	double DecelerationPeriod{0.5}; // part of the segment time allocated for deceleration
	double BlendingPeriod{0.0}; // the fraction of the segment's time to blend two linear segments of the trajectory
};

} //namespace RedundantRobot

#endif /* ROBOTMODEL_H_ */
