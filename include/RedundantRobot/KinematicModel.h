//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#ifndef KINEMATICMODEL_H_
#define KINEMATICMODEL_H_

#include <iostream>
#include <Eigen/Dense>
#include <Eigen/QR>
#include <Eigen/SVD>
#include "KinematicSolverData.h"

namespace RedundantRobot
{

class KinematicModel
{
public:
	KinematicModel( int DOF );
	
	KinematicModel( const KinematicModel &other );
	
	KinematicModel& operator= ( const KinematicModel &other );

	virtual ~KinematicModel();

	//~ Pure virtual functions -- subclasses have to provide their own functions
	virtual Eigen::MatrixXd GetJacobian( const Eigen::VectorXd &JointPos ) = 0; // computes the Jacobian of the manipulator
	//~ virtual Eigen::VectorXd GetEndEffectorVel( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &NextEndEffectorPos, double StepSize ) = 0; // computes the desired end effector velocity
	virtual Eigen::VectorXd GetEndEffectorError( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &EndEffectorPos ) = 0; // computes the end effector error
	virtual Eigen::MatrixXd GetObstacleJacobian( const Eigen::VectorXd &JointPos ) = 0; // computes the Jacobian of the part of the manipulator doing the obstacle avoidance task
	virtual Eigen::Vector3d ObstacleKinematics( const Eigen::VectorXd &JointPos ) = 0; // computes the location of the critical point (point avoiding the obstacle) on the robot

	//~ Computes one step of the inverse kinematics problem and returns joint positions, velocities and accelerations
	Eigen::MatrixXd InverseKinematicsStep( const Eigen::VectorXd &PrevJointPos, const Eigen::VectorXd &PrevJointVel, const Eigen::VectorXd &EndEffectorPos, const Eigen::VectorXd &EndEffectorVel, double StepSize );
	//~ Overloaded version for multistep ODE solvers
	Eigen::MatrixXd InverseKinematicsStep( const Eigen::VectorXd &PrevJointPos, const Eigen::VectorXd &PrevJointVel, const Eigen::VectorXd &EndEffectorPos, const Eigen::VectorXd &EndEffectorVel, const Eigen::VectorXd &MidEndEffectorVel, const Eigen::VectorXd &NextEndEffectorVel, double StepSize );
	
	//~ Compute the joint velocities vector
	Eigen::VectorXd GetJointVel( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &EndEffectorVel );
	
	//~ Compute joint velocities using one of the following integration methods:
	Eigen::VectorXd ExplicitMidpoint( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &EndEffectorError, const Eigen::VectorXd &EndEffectorVel, const Eigen::VectorXd &MidEndEffectorVel, double StepSize );
	
	Eigen::VectorXd ExplicitTrapezoidal( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &EndEffectorError, const Eigen::VectorXd &EndEffectorVel, const Eigen::VectorXd &NextEndEffectorVel, double StepSize );
	
	Eigen::VectorXd RungeKutta4( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &EndEffectorError, const Eigen::VectorXd &EndEffectorVel, const Eigen::VectorXd &MidEndEffectorVel, const Eigen::VectorXd &NextEndEffectorVel, double StepSize );
	
	//~ Computes the pseudoinverse of the jacobian matrix and the projector matrix for the subtask
	Eigen::MatrixXd JacobianPseudoInverseAndProjector( const Eigen::MatrixXd &Jacobian );
	
	//~ Pseudoinverses:
	void JacobianDamping( const Eigen::MatrixXd &Jacobian, Eigen::MatrixXd &InvertedJacobian, Eigen::MatrixXd &Projector );
	
	void SingularValueFiltering( const Eigen::MatrixXd &Jacobian, Eigen::MatrixXd &InvertedJacobian, Eigen::MatrixXd &Projector );
	
	void SingularValueFilteringModified( const Eigen::MatrixXd &Jacobian, Eigen::MatrixXd &InvertedJacobian, Eigen::MatrixXd &Projector );
	
	//~ Computes a vector representing a subtask (secondary priority to the main task) to make use of the redundancy of the kinematic chain
	Eigen::VectorXd GetNullSpaceVel( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &MainTaskJointVel, const Eigen::MatrixXd &Projector );
	
	//~ Subtasks:
	Eigen::VectorXd GetJointLimitGradient( const Eigen::VectorXd &JointPos );
	
	Eigen::VectorXd GetJointLimit( const Eigen::VectorXd &JointPos );
	
	Eigen::MatrixXd GetObstacleAvoidance( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &MainTaskJointVel, const Eigen::MatrixXd &Projector );
	
	//~ Allows to manage the parameters used in pseudoinversion of the Jacobian matrix
	void SetPseudoInverse( pinvType HowToInvertJacobian, double FirstValue = 0.0, double SecondValue = 0.0 );
	
	//~ Set the error gain for the CLIK method
	void SetErrorGain( double ErrorGain );
	
	//~ Set the saturation level of error for the CLIK method
	void SetErrorSaturation( double ErrorSaturation );
	
	//~ Set the integration method
	void SetIntegrationMethod( integrationType IntegrationMethod );
	
	//~ Get the type of the integration method
	integrationType GetIntegrationMethod();

	//~ Set the secondary task in the redundancy resolution
	void SetSubtask( subtaskType Subtask );
	
	//~ Get the type of the secondary task in the redundancy resolution.
	subtaskType GetSubtask();
	
	//~ Set the value of the subtask constraint gain (it scales the nullspace velocity)
	void SetSubtaskConstraintGain( double SubtaskConstraintGain );
	
	//~ Get the value of the subtask constraint gain
	double GetSubtaskConstraintGain();
	
	//~ Set the type of multitask formulation
	void SetMultitaskType( multitaskType MultitaskType );
	
	//~ Set the joint limits
	void SetJointLimits( const Eigen::VectorXd &JointLimitMax, const Eigen::VectorXd &JointLimitMin );
	
	//~ Set the obstacles' locations
	void SetObstaclePoints( const Eigen::MatrixXd ObstacleLocations );
	
	//~ Set the parameters of the obstacle avoidance subtask
	void SetObstacleAvoidanceParameters( double ObstacleSphereOfInfluence, double ObstacleUnityGain, double ObstacleAvoidanceNominalVel );
	
protected:
	int DOF; // number of degrees of freedom
	KinematicSolverData KinematicSettings;
};

} //namespace RedundantRobot

#endif /* KINEMATICMODEL_H_ */
