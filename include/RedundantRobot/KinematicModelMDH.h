//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#ifndef KINEMATICMODELMDH_H_
#define KINEMATICMODELMDH_H_

// Based on the class KinematicModel provides a forward kinematics solution and a jacobian, using Denavit-Hartenberg parameters

#include "KinematicModel.h"

//~ using RedundantRobot::KinematicModel;

namespace RedundantRobot
{

class KinematicModelMDH : public KinematicModel
{
public:
//	Constructor without an EE
	KinematicModelMDH( int DOF, const Eigen::VectorXd &MDHangles, const Eigen::VectorXd &MDHlengthsX, const Eigen::VectorXd &MDHlengthsZ );

//	Full parameterized constructor
	KinematicModelMDH( int DOF, const Eigen::VectorXd &MDHangles, const Eigen::VectorXd &MDHlengthsX, const Eigen::VectorXd &MDHlengthsZ, const Eigen::Vector3d &angles_EE, const Eigen::Vector3d &r_EE );

	KinematicModelMDH( const KinematicModelMDH &other );
	
	KinematicModelMDH& operator= ( const KinematicModelMDH &other );
	
	virtual ~KinematicModelMDH();

//	Forward kinematics
	Eigen::MatrixXd ForwardKinematics( const Eigen::VectorXd &JointPos );

// Implements pure virtual functions from the KinematicModel class
	virtual Eigen::MatrixXd GetJacobian( const Eigen::VectorXd &JointPos ); // computes the Jacobian of the manipulator
	//~ virtual Eigen::VectorXd GetEndEffectorVel( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &NextEndEffectorPos, double StepSize ); // computes the desired end effector velocity
	virtual Eigen::VectorXd GetEndEffectorError( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &EndEffectorPos ); // computes the end effector error
	virtual Eigen::MatrixXd GetObstacleJacobian( const Eigen::VectorXd &JointPos ); // computes the Jacobian of the part of the manipulator doing the obstacle avoidance task
	virtual Eigen::Vector3d ObstacleKinematics( const Eigen::VectorXd &JointPos ); // computes the location of the critical point (point avoiding the obstacle) on the robot

//	Calculates the transformation matrix from the last link frame to the end effector frame
	Eigen::MatrixXd EEFrame( const Eigen::Vector3d &angles_EE, const Eigen::Vector3d &r_EE );

//	Allows to change the location and orientation of the end effector frame relative to the last link frame
	void SetNewTool( const Eigen::Vector3d &New_angles_EE, const Eigen::Vector3d &New_r_EE );

//	Eigen::Matrix3d Skew( const Eigen::Vector3d &Vector );

protected:
	Eigen::VectorXd MDHangles;
	Eigen::VectorXd MDHlengthsX;
	Eigen::VectorXd MDHlengthsZ;
	Eigen::MatrixXd A_EE;

};

} //namespace RedundantRobot

#endif /* KINEMATICMODELMDH_H_ */
