function plots(i)

Q = dlmread("results/q.txt");
Q = Q * 180/pi;

dQ = dlmread("results/dq.txt");
dQ = dQ * 180/pi;

ddQ = dlmread("results/ddq.txt");
ddQ = ddQ * 180/pi;

desPos = dlmread("results/desPos.txt");
desVel = dlmread("results/desVel.txt");

Pos = dlmread("results/pos.txt");

Error = dlmread("results/posError.txt");

t = dlmread("results/time.txt");

WallClockTime = dlmread("results/WallClockTime.txt");

%plot(t,desPos(:,i),t,Pos(:,i)); legend("des", "msr"); xlabel("t, s"); ylabel("pos, m"); grid on;

%plot(t,Q(:,i));
%plot(t,dQ(:,i));
%plot(t,ddQ(:,i));
%plot(t,Q); legend("1", "2", "3", "4", "5", "6", "7"); xlabel("t, s"); ylabel("q, deg"); grid on;
plot(t,dQ); legend("1", "2", "3", "4", "5", "6", "7"); xlabel("t, s"); ylabel("dq, deg/s"); grid on;
%plot(t,ddQ); legend("1", "2", "3", "4", "5", "6", "7"); xlabel("t, s"); ylabel("ddq, deg/s^2"); grid on;
%plot(t,Error); legend("x", "y", "z"); xlabel("t, s"); ylabel("error, m"); grid on;
%plot(t,desPos); legend("x", "y", "z"); xlabel("t, s"); ylabel("pos, m"); grid on;
%plot3(desPos(:,1),desPos(:,2),desPos(:,3)); xlabel("x, m"); ylabel("y, m"); zlabel("z, m"); grid on;
%plot(t,desVel); legend("x", "y", "z"); xlabel("t, s"); ylabel("vel, m/s"); grid on;
%plot(t,WallClockTime); xlabel("t, s"); ylabel("Wall clock time, microsec"); grid on;
end