//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "RedundantRobot/RobotModel.h"
#include <iostream>
#include <fstream>

//const double pi = M_PI;

//~ TODO: run different pseudoinverses (to check how they behave near singularities) and save the results for comparison

//~ using namespace RedundantRobot;

void SaveJnt(const std::string &OutputFileName, const Eigen::MatrixXd &Jnt)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << Jnt;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
};

void SaveEE(const std::string &OutputFileName, const Eigen::MatrixXd &EE)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << EE;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
};

int main()
{
	const int DOF = 7;
	
	//~ Create a robot with given EE
	Eigen::Vector3d angles_EE(0.0, pi/2, pi/2);
	Eigen::Vector3d r_EE(0.0, 0.0, 0.078);
	RedundantRobot::RobotModel LWRRobot( angles_EE, r_EE );

	//~ Initialize the joint positions, velocities and accelerations
	Eigen::VectorXd q(DOF);
	q << 0.0, 0.0, 0.0, -M_PI/2.0, 0.0, M_PI/2.0, 0.0;
	Eigen::VectorXd dq = Eigen::VectorXd::Zero(DOF);
	Eigen::VectorXd ddq = Eigen::VectorXd::Zero(DOF);

	//~ Get the initial EE position
	Eigen::Vector3d P0 = LWRRobot.ForwardKinematics(q).block<3,1>(0,3);
	
	//~ Prepare the data for the trajectory generator
	Eigen::RowVectorXd SegmentTimes(4);
	SegmentTimes(0) = 0.0;
	SegmentTimes(1) = 3.0;
	SegmentTimes(2) = 3.0;
	SegmentTimes(3) = 3.0;
	Eigen::MatrixXd Points(3,4);
	Eigen::VectorXd qSingular(DOF);
	qSingular << 0.0, -M_PI/4.0, 0.0, 0.0, 0.0, 0.0, 0.0;
	Eigen::Vector3d P1 = LWRRobot.ForwardKinematics(qSingular).block<3,1>(0,3);
	Eigen::VectorXd qSingular2(DOF);
	qSingular2 << -M_PI/4.0, -M_PI/4.0, 0.0, 0.0, 0.0, 0.0, 0.0;
	Eigen::Vector3d P2 = LWRRobot.ForwardKinematics(qSingular2).block<3,1>(0,3);
	Eigen::VectorXd qFinal(DOF);
	qFinal << -M_PI/4.0, 0.0, 0.0, -M_PI/2.0, 0.0, M_PI/2.0, 0.0;
	Eigen::Vector3d P3 = LWRRobot.ForwardKinematics(qFinal).block<3,1>(0,3);
	Points.col(0) = P0;
	Points.col(1) = P1;
	Points.col(2) = P2;
	Points.col(3) = P3;
	double acc = 0.2;
	double blend = 0.2;
	LWRRobot.SetAccelerationDecelerationAndBlendingPeriod(acc, acc, blend);
	
	//~ Initialize the time stamps
	double dt = 0.005;
	double StartTime = 0.0;
	double EndTime = SegmentTimes(1);
	for (int k = 2; k < SegmentTimes.cols(); ++k)
	{
		EndTime += (1.0 - blend) * SegmentTimes(k);
	}
	
	//~ Set the method for inverting the Jacobian matrix; set the subtask; set the error gain for CLIK
	//~ LWRRobot.SetPseudoInverse(RedundantRobot::SVFmod, 0.005, 0.07);
	//~ LWRRobot.SetPseudoInverse(RedundantRobot::SVF, 0.01, 10.0);
	//~ LWRRobot.SetPseudoInverse(RedundantRobot::JD, 0.005, 0.07);
	//~ LWRRobot.SetPseudoInverse(RedundantRobot::JT);
	LWRRobot.SetPseudoInverse(RedundantRobot::JP);
	LWRRobot.SetSubtask(RedundantRobot::None);
	LWRRobot.SetErrorGain(1.0/dt);
	
	//~ Matrices to store the results
	Eigen::MatrixXd TotalResults;
	Eigen::VectorXd Time;
	Eigen::MatrixXd Q;
	Eigen::MatrixXd DQ;
	Eigen::MatrixXd DDQ;
	Eigen::MatrixXd TRQ;
	Eigen::MatrixXd DesiredEEPos;
	Eigen::MatrixXd DesiredEEVel;
	Eigen::MatrixXd EEPos;
	Eigen::MatrixXd PosError;
	Eigen::VectorXd WallClockTime;
	
	//~ Solve the inverse kinematics
	TotalResults = LWRRobot.SolveInverseKinematics(q, dq, Points, SegmentTimes, StartTime, EndTime, dt);
	int steps = TotalResults.rows();
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	DesiredEEPos = TotalResults.block(0, 1 + 3 * DOF, steps, 3);
	DesiredEEVel = TotalResults.block(0, 4 + 3 * DOF, steps, 3);
	WallClockTime = TotalResults.block(0, 7 + 3 * DOF, steps, 1);
	
	EEPos = Eigen::MatrixXd::Zero(steps,3);
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Solve the inverse dynamics
	TRQ = LWRRobot.SolveInverseDynamics(TotalResults.block(0, 1, steps, 3 * DOF));
	
	//~ Save the results to files
	SaveJnt("../results/q.txt", Q);
	SaveJnt("../results/dq.txt", DQ);
	SaveJnt("../results/ddq.txt", DDQ);
	SaveJnt("../results/trq.txt", TRQ);
	
	SaveEE("../results/desPos.txt", DesiredEEPos);
	SaveEE("../results/desVel.txt", DesiredEEVel);
	SaveEE("../results/pos.txt", EEPos);
	SaveEE("../results/posError.txt", PosError);
	SaveEE("../results/time.txt", Time);
	SaveEE("../results/WallClockTime.txt", WallClockTime);
	
	return 0;
}
