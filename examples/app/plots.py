#~ Copyright (C) 2019 Łukassz Woliński
#~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

import numpy as np
import matplotlib.pyplot as plt

file_contents = open("results/q.txt", "r");
Q = [ [float(num) for num in line.split()] for line in file_contents]
plt.plot(Q, "-")
#~ plt.show()
plt.grid()
plt.savefig("Q.pdf")
plt.clf()

file_contents = open("results/dq.txt", "r");
DQ = [ [float(num) for num in line.split()] for line in file_contents]
plt.plot(DQ, "-")
plt.grid()
plt.savefig("DQ.pdf")
plt.clf()

file_contents = open("results/ddq.txt", "r");
DDQ = [ [float(num) for num in line.split()] for line in file_contents]
plt.plot(DDQ, "-")
plt.grid()
plt.savefig("DDQ.pdf")
plt.clf()

file_contents = open("results/trq.txt", "r");
TRQ = [ [float(num) for num in line.split()] for line in file_contents]
plt.plot(TRQ, "-")
plt.grid()
plt.savefig("TRQ.pdf")
plt.clf()

file_contents = open("results/pos.txt", "r");
X = [ [float(num) for num in line.split()] for line in file_contents]
plt.plot(X, "-")
plt.grid()
plt.savefig("pos.pdf");
plt.clf()

file_contents = open("results/desPos.txt", "r");
desX = [ [float(num) for num in line.split()] for line in file_contents]
plt.plot(desX, "-")
plt.grid()
plt.savefig("desPos.pdf");
plt.clf()

file_contents = open("results/posError.txt", "r");
E = [ [float(num) for num in line.split()] for line in file_contents]
plt.plot(E, "-")
plt.grid()
plt.savefig("posError.pdf")
plt.clf()
