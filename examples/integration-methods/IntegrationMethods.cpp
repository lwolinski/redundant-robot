//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "RedundantRobot/RobotModel.h"
#include <iostream>
#include <fstream>

//const double pi = M_PI;

//~ using namespace RedundantRobot;

void SaveJnt(const std::string &OutputFileName, const Eigen::MatrixXd &Jnt)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << Jnt;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
};

void SaveEE(const std::string &OutputFileName, const Eigen::MatrixXd &EE)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << EE;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
};

int main()
{
	const int DOF = 7;
	
	//~ Create a robot with given EE
	Eigen::Vector3d angles_EE(0.0, pi/2, pi/2);
	Eigen::Vector3d r_EE(0.1, 0.0, 0.078);
	RedundantRobot::RobotModel LWRRobot( angles_EE, r_EE );

	//~ Initialize the joint positions, velocities and accelerations
	Eigen::VectorXd q(DOF);
	q << 0.0, 0.0, 0.0, -pi/2.0, 0.0, pi/2.0, 0.0;
	Eigen::VectorXd dq = Eigen::VectorXd::Zero(DOF);
	Eigen::VectorXd ddq = Eigen::VectorXd::Zero(DOF);

	//~ Get the initial EE position
	Eigen::Vector3d StartEEPos = LWRRobot.ForwardKinematics(q).block<3,1>(0,3);
	//~ Get the target EE position
	Eigen::VectorXd q1(7);
	q1 << -pi/2.0, 0.0, 0.0, pi/2.0, 0.0, -pi/2.0, 0.0;
	Eigen::Vector3d EndEEPos = LWRRobot.ForwardKinematics(q1).block<3,1>(0,3);
	
	//~ Initialize the time stamps
	double dt = 0.1;
	double StartTime = 0.0;
	double EndTime = 1.0;
	
	//~ Prepare the data for the trajectory generator
	Eigen::RowVectorXd SegmentTimes(2);
	SegmentTimes(0) = StartTime;
	SegmentTimes(1) = EndTime;
	Eigen::MatrixXd Points(3,2);
	Points.col(0) = StartEEPos;
	Points.col(1) = EndEEPos;
	
	//~ Set the method for inverting the Jacobian matrix; set the subtask; set the error gain for CLIK
	LWRRobot.SetPseudoInverse(RedundantRobot::SVFmod, 0.04, 0.001);
	LWRRobot.SetSubtask(RedundantRobot::None);
	LWRRobot.SetErrorGain(1.0/dt);
	//~ LWRRobot.SetErrorSaturation(0.01);
	
	//~ Matrices to store the results
	Eigen::MatrixXd TotalResults;
	Eigen::VectorXd Time;
	Eigen::MatrixXd Q;
	Eigen::MatrixXd DQ;
	Eigen::MatrixXd DDQ;
	Eigen::MatrixXd TRQ;
	Eigen::MatrixXd DesiredEEPos;
	Eigen::MatrixXd EEPos;
	Eigen::MatrixXd PosError;
	
	//~ Solve the inverse kinematics
	TotalResults = LWRRobot.SolveInverseKinematics(q, dq, Points, SegmentTimes, StartTime, EndTime, dt);
	int steps = TotalResults.rows();
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	DesiredEEPos = TotalResults.block(0, 1 + 3 * DOF, steps, 3);
	
	EEPos = Eigen::MatrixXd::Zero(steps,3);
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Save the results to files
	SaveJnt("../results/qEE.txt", Q);
	SaveJnt("../results/dqEE.txt", DQ);
	SaveJnt("../results/ddqEE.txt", DDQ);
	
	SaveEE("../results/desPosEE.txt", DesiredEEPos);
	SaveEE("../results/posEE.txt", EEPos);
	SaveEE("../results/posErrorEE.txt", PosError);
	SaveEE("../results/time.txt", Time);
	
	//~ Once more, with different integration method
	LWRRobot.SetIntegrationMethod(RedundantRobot::EM);
	//~ Solve the inverse kinematics
	TotalResults = LWRRobot.SolveInverseKinematics(q, dq, Points, SegmentTimes, StartTime, EndTime, dt);
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	DesiredEEPos = TotalResults.block(0, 1 + 3 * DOF, steps, 3);
	
	EEPos = Eigen::MatrixXd::Zero(steps,3);
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Save the results to files
	SaveJnt("../results/qEM.txt", Q);
	SaveJnt("../results/dqEM.txt", DQ);
	SaveJnt("../results/ddqEM.txt", DDQ);
	
	SaveEE("../results/desPosEM.txt", DesiredEEPos);
	SaveEE("../results/posEM.txt", EEPos);
	SaveEE("../results/posErrorEM.txt", PosError);
	
	//~ Once more, with different integration method
	LWRRobot.SetIntegrationMethod(RedundantRobot::ET);
	//~ Solve the inverse kinematics
	TotalResults = LWRRobot.SolveInverseKinematics(q, dq, Points, SegmentTimes, StartTime, EndTime, dt);
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	DesiredEEPos = TotalResults.block(0, 1 + 3 * DOF, steps, 3);
	
	EEPos = Eigen::MatrixXd::Zero(steps,3);
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Save the results to files
	SaveJnt("../results/qET.txt", Q);
	SaveJnt("../results/dqET.txt", DQ);
	SaveJnt("../results/ddqET.txt", DDQ);
	
	SaveEE("../results/desPosET.txt", DesiredEEPos);
	SaveEE("../results/posET.txt", EEPos);
	SaveEE("../results/posErrorET.txt", PosError);
	
	//~ Once more, with different integration method
	LWRRobot.SetIntegrationMethod(RedundantRobot::RK4);
	//~ Solve the inverse kinematics
	TotalResults = LWRRobot.SolveInverseKinematics(q, dq, Points, SegmentTimes, StartTime, EndTime, dt);
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	DesiredEEPos = TotalResults.block(0, 1 + 3 * DOF, steps, 3);
	
	EEPos = Eigen::MatrixXd::Zero(steps,3);
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Save the results to files
	SaveJnt("../results/qRK4.txt", Q);
	SaveJnt("../results/dqRK4.txt", DQ);
	SaveJnt("../results/ddqRK4.txt", DDQ);
	
	SaveEE("../results/desPosRK4.txt", DesiredEEPos);
	SaveEE("../results/posRK4.txt", EEPos);
	SaveEE("../results/posErrorRK4.txt", PosError);
	
	return 0;
}
