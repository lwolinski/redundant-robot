function plots(i)

QEE = dlmread("results/qEE.txt");
QEE = QEE * 180/pi;

dQEE = dlmread("results/dqEE.txt");
dQEE = dQEE * 180/pi;

ddQEE = dlmread("results/ddqEE.txt");
ddQEE = ddQEE * 180/pi;

desPos = dlmread("results/desPosEE.txt");

desVel = dlmread("results/desVelEE.txt");

PosEE = dlmread("results/posEE.txt");

ErrorEE = dlmread("results/posErrorEE.txt");

QEM = dlmread("results/qEM.txt");
QEM = QEM * 180/pi;

dQEM = dlmread("results/dqEM.txt");
dQEM = dQEM * 180/pi;

ddQEM = dlmread("results/ddqEM.txt");
ddQEM = ddQEM * 180/pi;

desPosEM = dlmread("results/desPosEM.txt");

PosEM = dlmread("results/posEM.txt");

ErrorEM = dlmread("results/posErrorEM.txt");

QET = dlmread("results/qET.txt");
QET = QET * 180/pi;

dQET = dlmread("results/dqET.txt");
dQET = dQET * 180/pi;

ddQET = dlmread("results/ddqET.txt");
ddQET = ddQET * 180/pi;

desPosET = dlmread("results/desPosET.txt");

PosET = dlmread("results/posET.txt");

ErrorET = dlmread("results/posErrorET.txt");

QRK4 = dlmread("results/qRK4.txt");
QRK4 = QRK4 * 180/pi;

dQRK4 = dlmread("results/dqRK4.txt");
dQRK4 = dQRK4 * 180/pi;

ddQRK4 = dlmread("results/ddqRK4.txt");
ddQRK4 = ddQRK4 * 180/pi;

desPosRK4 = dlmread("results/desPosRK4.txt");

PosRK4 = dlmread("results/posRK4.txt");

ErrorRK4 = dlmread("results/posErrorRK4.txt");

t = dlmread("results/time.txt");

%plot(t,desPos,t,PosEE);

%plot(t,Q(:,i));
%plot(t,dQ(:,i));
%plot(t,ddQ(:,i));
%plot(t,Q);
%plot(t,dQRK4);
%plot(t,ddQ);
plot(t,ErrorEE(:,i),t,ErrorEM(:,i),t,ErrorET(:,i),t,ErrorRK4(:,i)); legend("EE", "EM", "ET", "RK4"); grid on;
%plot(t,QEE(:,i),t,QEM(:,i),t,QET(:,i),t,QRK4(:,i));
%plot(t,desPos(:,i),t,PosEE(:,i),t,PosEM(:,i),t,PosET(:,i),t,PosRK4(:,i)); legend("des", "EE", "EM", "ET", "RK4"); grid on;
%plot(t,desPos(:,i),t,desVel(:,i)); legend("pos", "vel"); grid on;
end