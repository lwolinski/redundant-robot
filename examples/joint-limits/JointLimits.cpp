//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "RedundantRobot/RobotModel.h"
#include <iostream>
#include <fstream>

void SaveJnt(const std::string &OutputFileName, const Eigen::MatrixXd &Jnt)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << Jnt;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
}

void SaveEE(const std::string &OutputFileName, const Eigen::MatrixXd &EE)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << EE;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
}

int main()
{
	const int DOF = 7;
	
	//~ Create a robot with given EE
	Eigen::Vector3d angles_EE(0.0, 0.0, 0.0);
	Eigen::Vector3d r_EE(0.0, 0.0, 0.088);
	RedundantRobot::RobotModel LWRRobot( angles_EE, r_EE );

	//~ Initialize the joint positions, velocities and accelerations
	Eigen::VectorXd q(DOF);
	q << -16.19, 65.0, -16.56, 67.0, -13.25, -54.12, 0.0;
	q *= (pi/180);
	Eigen::VectorXd dq = Eigen::VectorXd::Zero(DOF);
	Eigen::VectorXd ddq = Eigen::VectorXd::Zero(DOF);

	std::cout << std::endl << LWRRobot.ForwardKinematics( q ) << std::endl << std::endl;

	//~ Get the initial EE position
	Eigen::Vector3d StartEEPos = LWRRobot.ForwardKinematics(q).block<3,1>(0,3);
	//~ Declare the final EE position
	Eigen::Vector3d EndEEPos(0.6, 0.1, 0.6);
	
	//~ Initialize the time stamps
	double dt = 0.002;
	//~ double dt = 0.02;
	//~ double dt = 0.1;
	double StartTime = 0.0;
	double EndTime = 5.0;
	
	//~ Prepare the data for the trajectory generator
	Eigen::RowVectorXd SegmentTimes(2);
	SegmentTimes(0) = StartTime;
	SegmentTimes(1) = EndTime;
	Eigen::MatrixXd Points(3,2);
	Points.col(0) = StartEEPos;
	Points.col(1) = EndEEPos;
	
	//~ Set the method for inverting the Jacobian matrix; set the subtask; set the error gain for CLIK; set the integration method
	LWRRobot.SetPseudoInverse(RedundantRobot::SVFmod, 0.04, 0.001);
	LWRRobot.SetSubtask(RedundantRobot::None);
	LWRRobot.SetErrorGain(1.0/dt);
	LWRRobot.SetIntegrationMethod(RedundantRobot::EE);
	//~ LWRRobot.SetIntegrationMethod(RedundantRobot::RK4);
	
	//~ Matrices to store the results
	Eigen::MatrixXd TotalResults;
	Eigen::VectorXd Time;
	Eigen::MatrixXd Q;
	Eigen::MatrixXd DQ;
	Eigen::MatrixXd DDQ;
	Eigen::MatrixXd DesiredEEPos;
	Eigen::MatrixXd DesiredEEVel;
	Eigen::MatrixXd EEPos;
	Eigen::MatrixXd PosError;
	Eigen::VectorXd WallClockTime;
	
	//~ Solve the inverse kinematics
	TotalResults = LWRRobot.SolveInverseKinematics(q, dq, Points, SegmentTimes, StartTime, EndTime, dt);
	int steps = TotalResults.rows();
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	DesiredEEPos = TotalResults.block(0, 1 + 3 * DOF, steps, 3);
	DesiredEEVel = TotalResults.block(0, 4 + 3 * DOF, steps, 3);
	WallClockTime = TotalResults.block(0, 7 + 3 * DOF, steps, 1);
	
	EEPos = Eigen::MatrixXd::Zero(steps,3);
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Save the results to files
	SaveJnt("../results/q.txt", Q);
	SaveJnt("../results/dq.txt", DQ);
	SaveJnt("../results/ddq.txt", DDQ);
	
	SaveEE("../results/desPos.txt", DesiredEEPos);
	SaveEE("../results/desVel.txt", DesiredEEVel);
	SaveEE("../results/pos.txt", EEPos);
	SaveEE("../results/posError.txt", PosError);
	SaveEE("../results/time.txt", Time);
	SaveEE("../results/WallClockTime.txt", WallClockTime);
	
	//~ Once more, this time with joint limits
	LWRRobot.SetSubtask(RedundantRobot::JointLimit);
	LWRRobot.SetSubtaskConstraintGain(0.02);
	
	//~ Set the joint limits
	Eigen::VectorXd JointMax(DOF);
	JointMax << 140.0, 90.0, 10.0, 110.0, 90.0, 70.0, 45.0;
	JointMax *= (pi/180.0);
	Eigen::VectorXd JointMin(DOF);
	JointMin << -45.0, -90.0, -160.0, 0.0, -45.0, -90.0, -45.0;
	JointMin *= (pi/180.0);
	LWRRobot.SetJointLimits(JointMax, JointMin);
	
	TotalResults = LWRRobot.SolveInverseKinematics(q, dq, Points, SegmentTimes, StartTime, EndTime, dt);
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	WallClockTime = TotalResults.block(0, 7 + 3 * DOF, steps, 1);
	
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Save the results to files
	SaveJnt("../results/qJntLim.txt", Q);
	SaveJnt("../results/dqJntLim.txt", DQ);
	SaveJnt("../results/ddqJntLim.txt", DDQ);
	
	SaveEE("../results/desPosJntLim.txt", DesiredEEPos);
	SaveEE("../results/posJntLim.txt", EEPos);
	SaveEE("../results/posErrorJntLim.txt", PosError);
	SaveEE("../results/WallClockTimeLim.txt", WallClockTime);
	
	return 0;
}
