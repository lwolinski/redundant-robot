//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "RedundantRobot/RobotModel.h"
#include <iostream>
#include <fstream>

//const double pi = M_PI;

//~ using namespace RedundantRobot;

void SaveJnt(const std::string &OutputFileName, const Eigen::MatrixXd &Jnt)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << Jnt;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
};

void SaveEE(const std::string &OutputFileName, const Eigen::MatrixXd &EE)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << EE;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
};

int main()
{
	const int DOF = 7;
	
	//~ Create a robot with given EE
	Eigen::Vector3d angles_EE(0.0, pi/2, pi/2);
	Eigen::Vector3d r_EE(0.1, 0.0, 0.078);
	RedundantRobot::RobotModel LWRRobot( angles_EE, r_EE );

	//~ Initialize the joint positions, velocities and accelerations
	Eigen::VectorXd q(DOF);
	q << 0.0, 0.0, 0.0, -pi/2.0, 0.0, pi/2.0, 0.0;
	Eigen::VectorXd dq = Eigen::VectorXd::Zero(DOF);
	Eigen::VectorXd ddq = Eigen::VectorXd::Zero(DOF);

	//~ Get the initial EE position
	Eigen::Vector3d P0 = LWRRobot.ForwardKinematics(q).block<3,1>(0,3);

	//~ Initialize the time stamps
	double dt = 0.005;
	double StartTime = 0.0;
	double EndTime = 5.0;
	
	//~ Prepare the data for the trajectory generator
	Eigen::RowVectorXd SegmentTimes(6);
	SegmentTimes(0) = 0.0;
	SegmentTimes(1) = 0.9;
	SegmentTimes(2) = 1.2;
	SegmentTimes(3) = 0.7;
	SegmentTimes(4) = 1.1;
	SegmentTimes(5) = 1.1;
	Eigen::MatrixXd Points(3,6);
	Eigen::Vector3d P1(-0.4, 0.2, 0.5);
	Eigen::Vector3d P2(-0.4, -0.2, 0.5);
	Eigen::Vector3d P3(-0.65, -0.2, 0.5);
	Eigen::Vector3d P4(-0.65, 0.2, 0.5);
	Points.col(0) = P0;
	Points.col(1) = P1;
	Points.col(2) = P2;
	Points.col(3) = P3;
	Points.col(4) = P4;
	Points.col(5) = P1;
	
	//~ Set the method for inverting the Jacobian matrix; set the subtask; set the error gain for CLIK
	LWRRobot.SetPseudoInverse(RedundantRobot::JD, 0.04, 0.001);
	LWRRobot.SetSubtask(RedundantRobot::ObstacleAvoidance);
	LWRRobot.SetMultitaskType(RedundantRobot::Standard);
	LWRRobot.SetErrorGain(1.0/dt);
	
	//~ Matrices to store the results
	Eigen::MatrixXd TotalResults;
	Eigen::VectorXd Time;
	Eigen::MatrixXd Q;
	Eigen::MatrixXd DQ;
	Eigen::MatrixXd DDQ;
	Eigen::MatrixXd DesiredEEPos;
	Eigen::MatrixXd DesiredEEVel;
	Eigen::MatrixXd EEPos;
	Eigen::MatrixXd PosError;
	Eigen::VectorXd ObstacleDistance;
	Eigen::VectorXd WallClockTime;
	
	//~ Define the obstacle
	Eigen::MatrixXd ObstacleLocations = Eigen::Vector3d(-0.5, 0.0, 0.5);
	//~ Set the parameters for obstacle avoidance
	double ObstacleSphereOfInfluence = 0.4;
	double ObstacleUnityGain = 0.35;
	double ObstacleAvoidanceNominalVel = 100.0;//0.3;
	LWRRobot.SetObstacleAvoidanceParameters(ObstacleSphereOfInfluence, ObstacleUnityGain, ObstacleAvoidanceNominalVel);
	
	//~ Solve the inverse kinematics
	TotalResults = LWRRobot.SolveInverseKinematics(q, dq, Points, SegmentTimes, StartTime, EndTime, dt, ObstacleLocations);
	int steps = TotalResults.rows();
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	DesiredEEPos = TotalResults.block(0, 1 + 3 * DOF, steps, 3);
	DesiredEEVel = TotalResults.block(0, 4 + 3 * DOF, steps, 3);
	ObstacleDistance = TotalResults.block(0, 7 + 3 * DOF, steps, 1);
	WallClockTime = TotalResults.block(0, 8 + 3 * DOF, steps, 1);
	
	EEPos = Eigen::MatrixXd::Zero(steps,3);
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Save the results to files
	SaveJnt("../results/q.txt", Q);
	SaveJnt("../results/dq.txt", DQ);
	SaveJnt("../results/ddq.txt", DDQ);
	
	SaveEE("../results/desPos.txt", DesiredEEPos);
	SaveEE("../results/pos.txt", EEPos);
	SaveEE("../results/posError.txt", PosError);
	SaveEE("../results/ObstacleDistance.txt", ObstacleDistance);
	SaveEE("../results/time.txt", Time);
	
	//~ Solve the inverse kinematics once more, with a different method for task priority
	LWRRobot.SetSubtask(RedundantRobot::ObstacleAvoidance);
	LWRRobot.SetMultitaskType(RedundantRobot::SingularityRobust);
	TotalResults = LWRRobot.SolveInverseKinematics(q, dq, Points, SegmentTimes, StartTime, EndTime, dt, ObstacleLocations);
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	DesiredEEPos = TotalResults.block(0, 1 + 3 * DOF, steps, 3);
	DesiredEEVel = TotalResults.block(0, 4 + 3 * DOF, steps, 3);
	ObstacleDistance = TotalResults.block(0, 7 + 3 * DOF, steps, 1);
	WallClockTime = TotalResults.block(0, 8 + 3 * DOF, steps, 1);
	
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Save the results to files
	SaveJnt("../results/q2.txt", Q);
	SaveJnt("../results/dq2.txt", DQ);
	SaveJnt("../results/ddq2.txt", DDQ);
	
	SaveEE("../results/pos2.txt", EEPos);
	SaveEE("../results/posError2.txt", PosError);
	SaveEE("../results/ObstacleDistance2.txt", ObstacleDistance);
	
	//~ Solve the inverse kinematics once more, without considering the obstacle avoidance task
	LWRRobot.SetSubtask(RedundantRobot::None);
	TotalResults = LWRRobot.SolveInverseKinematics(q, dq, Points, SegmentTimes, StartTime, EndTime, dt, ObstacleLocations);
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	DesiredEEPos = TotalResults.block(0, 1 + 3 * DOF, steps, 3);
	DesiredEEVel = TotalResults.block(0, 4 + 3 * DOF, steps, 3);
	ObstacleDistance = TotalResults.block(0, 7 + 3 * DOF, steps, 1);
	WallClockTime = TotalResults.block(0, 8 + 3 * DOF, steps, 1);
	
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Save the results to files
	SaveJnt("../results/q3.txt", Q);
	SaveJnt("../results/dq3.txt", DQ);
	SaveJnt("../results/ddq3.txt", DDQ);
	
	SaveEE("../results/pos3.txt", EEPos);
	SaveEE("../results/posError3.txt", PosError);
	SaveEE("../results/ObstacleDistance3.txt", ObstacleDistance);
	
	return 0;
}
