//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "RedundantRobot/RobotModel.h"
#include <iostream>
#include <fstream>

void SaveJnt(const std::string &OutputFileName, const Eigen::MatrixXd &Jnt)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << Jnt;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
};

void SaveEE(const std::string &OutputFileName, const Eigen::MatrixXd &EE)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << EE;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
};

Eigen::MatrixXd ReadPoints(const std::string& filename)
{
	//~ Each row is a pair of points in the 7-DOF manipulator's joint space, so 14 elements in each row.
	
	const int MaxBufferSize = 20000;
	
	std::ifstream FileToRead;

	FileToRead.open(filename);

	if(!FileToRead)
	{
		std::cout << "Could not open a file." << std::endl;
	}
	
	//~ Read the file into the buffer
	double TempBuffer[MaxBufferSize];
	int k = 0;
	while(!FileToRead.eof())
	{
		FileToRead >> TempBuffer[k];
		k++;
	}
	
	//~ Read the positions from the buffer into the (k / 14) * 14 matrix
	Eigen::MatrixXd JointPoints( k/14, 14 );
	for (int i = 0; i < (k/14); ++i)
	{
		for (int j = 0; j < 14; ++j)
		{
			JointPoints(i, j) = TempBuffer[14 * i + j];
		}
	}
	
	return JointPoints;
}

int main()
{
	const int DOF = 7;
	
	//~ Read the joint space points from file
	Eigen::MatrixXd JointPoints = ReadPoints("../Points.txt");
	
	//~ Create a robot with given EE
	Eigen::Vector3d angles_EE(0.0, 0.0, 0.0);
	Eigen::Vector3d r_EE(0.1, 0.0, 0.078);
	RedundantRobot::RobotModel LWRRobot( angles_EE, r_EE );

	//~ Initialize the joint positions, velocities and accelerations
	int k = 7; //4, 7
	Eigen::VectorXd JointPosStart = JointPoints.block(k, 0, 1, DOF).transpose() * (M_PI / 180.0);
	Eigen::VectorXd JointPosEnd = JointPoints.block(k, DOF, 1, DOF).transpose() * (M_PI / 180.0);
	Eigen::VectorXd JointVelStart = Eigen::VectorXd::Zero(DOF);
	
	//~ Get the initial and final EE position
	Eigen::Vector3d StartEEPos = LWRRobot.ForwardKinematics(JointPosStart).block<3,1>(0,3);
	Eigen::Vector3d EndEEPos = LWRRobot.ForwardKinematics(JointPosEnd).block<3,1>(0,3);
	
	double MaxVel = 1.0;
	double TrajectoryLength = (EndEEPos - StartEEPos).norm();
	double AccelerationPeriod = 0.5;
	double DecelerationPeriod = 0.5;
	LWRRobot.SetAccelerationAndDecelerationPeriod(AccelerationPeriod, DecelerationPeriod);
	
	//~ Initialize the time stamps
	double StartTime = 0.0;
	double EndTime = 2.0 * TrajectoryLength / ((2.0 - AccelerationPeriod - DecelerationPeriod) * MaxVel);
	double dt = 0.002;
	
	//~ Prepare the data for the trajectory generator
	Eigen::RowVectorXd SegmentTimes(2);
	SegmentTimes(0) = StartTime;
	SegmentTimes(1) = EndTime;
	Eigen::MatrixXd Points(3,2);
	Points.col(0) = StartEEPos;
	Points.col(1) = EndEEPos;
	
	//~ Set the method for inverting the Jacobian matrix; set the subtask; set the error gain for CLIK; set the integration method
	LWRRobot.SetPseudoInverse(RedundantRobot::SVFmod, 0.04, 0.001);
	LWRRobot.SetSubtask(RedundantRobot::None);
	LWRRobot.SetErrorGain(1.0/dt);
	LWRRobot.SetIntegrationMethod(RedundantRobot::EE);
	//~ LWRRobot.SetIntegrationMethod(RedundantRobot::RK4);
	
	//~ Matrices to store the results
	Eigen::MatrixXd TotalResults;
	Eigen::VectorXd Time;
	Eigen::MatrixXd Q;
	Eigen::MatrixXd DQ;
	Eigen::MatrixXd DDQ;
	Eigen::MatrixXd DesiredEEPos;
	Eigen::MatrixXd DesiredEEVel;
	Eigen::MatrixXd EEPos;
	Eigen::MatrixXd PosError;
	Eigen::VectorXd WallClockTime;
	
	//~ Solve the inverse kinematics
	TotalResults = LWRRobot.SolveInverseKinematics(JointPosStart, JointVelStart, Points, SegmentTimes, StartTime, EndTime, dt);
	int steps = TotalResults.rows();
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	DesiredEEPos = TotalResults.block(0, 1 + 3 * DOF, steps, 3);
	DesiredEEVel = TotalResults.block(0, 4 + 3 * DOF, steps, 3);
	WallClockTime = TotalResults.block(0, 7 + 3 * DOF, steps, 1);
	
	EEPos = Eigen::MatrixXd::Zero(steps,3);
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Save the results to files
	SaveJnt("../results/q.txt", Q);
	SaveJnt("../results/dq.txt", DQ);
	SaveJnt("../results/ddq.txt", DDQ);
	
	SaveEE("../results/desPos.txt", DesiredEEPos);
	SaveEE("../results/desVel.txt", DesiredEEVel);
	SaveEE("../results/pos.txt", EEPos);
	SaveEE("../results/posError.txt", PosError);
	SaveEE("../results/time.txt", Time);
	SaveEE("../results/WallClockTime.txt", WallClockTime);
	
	//~ Once more, this time with joint limits
	LWRRobot.SetSubtask(RedundantRobot::JointLimit);
	LWRRobot.SetMultitaskType(RedundantRobot::SwitchingPriorities);
	LWRRobot.SetSubtaskConstraintGain(0.2); //0.2
	
	//~ Set the joint limits
	Eigen::VectorXd JointLim(DOF);
	JointLim << 170.0, 120.0, 170.0, 120.0, 170.0, 120.0, 170.0;
	Eigen::VectorXd JointMax = JointLim * (M_PI / 180.0);
	Eigen::VectorXd JointMin = -JointLim * (M_PI / 180.0);
	LWRRobot.SetJointLimits(JointMax, JointMin);
	double JointLimitGain = 0.5;
	double JointLimitNearDistance = 10.0 * M_PI / 180.0;
	double JointLimitUnityGain = 5.0 * M_PI / 180.0;
	LWRRobot.SetHardJointLimitParameters(JointLimitGain, JointLimitNearDistance, JointLimitUnityGain);
	
	TotalResults = LWRRobot.SolveInverseKinematics(JointPosStart, JointVelStart, Points, SegmentTimes, StartTime, EndTime, dt);
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	WallClockTime = TotalResults.block(0, 7 + 3 * DOF, steps, 1);
	
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Save the results to files
	SaveJnt("../results/qJntLim.txt", Q);
	SaveJnt("../results/dqJntLim.txt", DQ);
	SaveJnt("../results/ddqJntLim.txt", DDQ);
	
	SaveEE("../results/desPosJntLim.txt", DesiredEEPos);
	SaveEE("../results/posJntLim.txt", EEPos);
	SaveEE("../results/posErrorJntLim.txt", PosError);
	SaveEE("../results/WallClockTimeLim.txt", WallClockTime);
	
	return 0;
}
