function plots(i)
QLim = dlmread("results/qJntLim.txt");
QLim = QLim * 180/pi;
Q = dlmread("results/q.txt");
Q = Q * 180/pi;
%plot(QLim(:,3));
%plot(QLim);

dQLim = dlmread("results/dqJntLim.txt");
dQLim = dQLim * 180/pi;
dQ = dlmread("results/dq.txt");
dQ = dQ * 180/pi;
%plot(dQLim(:,3));
%plot(dQLim);

ddQLim = dlmread("results/ddqJntLim.txt");
ddQLim = ddQLim * 180/pi;
ddQ = dlmread("results/ddq.txt");
ddQ = ddQ * 180/pi;

desPos = dlmread("results/desPosJntLim.txt");
desVel = dlmread("results/desVel.txt");

WallClockTime = dlmread("results/WallClockTime.txt");
WallClockTimeLim = dlmread("results/WallClockTimeLim.txt");

Pos = dlmread("results/posJntLim.txt");
%plot(Pos);

Error = dlmread("results/posErrorJntLim.txt");

t = dlmread("results/time.txt");
%plot(t,desPos,t,Pos);

%plot(t,Q(:,i),t,QLim(:,i)); legend("none", "joint limits"); xlabel("t, s"); ylabel("q, deg"); grid on;
%plot(t,dQ(:,i),t,dQLim(:,i)); legend("none", "joint limits"); xlabel("t, s"); ylabel("dq, deg/s"); grid on;
%plot(t,ddQ(:,i),t,ddQLim(:,i)); legend("none", "joint limits"); xlabel("t, s"); ylabel("ddq, deg/s^2"); grid on;
%plot(t,QLim); legend("1", "2", "3", "4", "5", "6", "7"); xlabel("t, s"); ylabel("q, deg"); grid on;
%plot(t,dQLim); legend("1", "2", "3", "4", "5", "6", "7"); xlabel("t, s"); ylabel("dq, deg/s"); grid on;
%plot(t,ddQLim);
plot(t,Error); legend("x", "y", "z"); xlabel("t, s"); ylabel("error, m"); grid on;
%plot(t,Pos); legend("x", "y", "z"); xlabel("t, s"); ylabel("pos, m"); grid on;
%plot(t,desVel); legend("x", "y", "z"); xlabel("t, s"); ylabel("vel, m/s"); grid on;
%plot(t,WallClockTime,t,WallClockTimeLim); legend("none", "joint limits"); xlabel("t, s"); ylabel("Wall clock time, microsec"); grid on;
end