//~ Copyright (C) 2024 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "RedundantRobot/RobotModel.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>

//const double pi = M_PI;

//~ using namespace RedundantRobot;

void SaveJnt(const std::string &OutputFileName, const Eigen::MatrixXd &Jnt)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << Jnt;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
};

void SaveEE(const std::string &OutputFileName, const Eigen::MatrixXd &EE)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << EE;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
};

int main()
{
    //This program generates random trajectory consisting of linear segments and solves the inverse kinematics problem.
    
    // seed
    srand (static_cast <unsigned> (time(0)));
    
	const int DOF = 7;
	
	//~ Create a robot with given EE
	Eigen::Vector3d angles_EE(0.0, pi/2, pi/2);
	Eigen::Vector3d r_EE(0.1, 0.0, 0.078);
	RedundantRobot::RobotModel LWRRobot( angles_EE, r_EE );

	//~ Initialize the joint positions, velocities and accelerations
	Eigen::VectorXd q0(DOF);
	q0 << 0.0, 0.0, 0.0, -90.0, 0.0, 90.0, 0.0; q0 *= M_PI / 180.0;
	Eigen::VectorXd dq0 = Eigen::VectorXd::Zero(DOF);
	Eigen::VectorXd ddq0 = Eigen::VectorXd::Zero(DOF);

	//~ Get the initial EE position
	Eigen::Vector3d P0 = LWRRobot.ForwardKinematics(q0).block<3,1>(0,3);
	
	//~ Prepare the data for the trajectory generator
    int numberOfSegments = 4;
    double maxVel = 1.0; // max vel along a linear segment in m/s
	Eigen::RowVectorXd SegmentTimes(numberOfSegments + 1);
	Eigen::MatrixXd Points(3, numberOfSegments + 1);
    Points.col(0) = P0;
    SegmentTimes(0) = 0.0;
    for (int i = 1; i <= numberOfSegments; ++i)
    {
        Eigen::VectorXd q(DOF);
        for (int j = 0; j <= 3; ++j)
        {
            double min = -165.0;
            double max = 165.0;
            q(2 * j) = min + static_cast <double> (rand()) /( static_cast <double> (RAND_MAX / (max - min)) );
        }
        for (int j = 0; j <= 2; ++j)
        {
            double min = -115;
            double max = 115;
            q(2 * j + 1) = min + static_cast <double> (rand()) /( static_cast <double> (RAND_MAX / (max - min)) );
        }
        q *= M_PI / 180.0;
        Points.col(i) = LWRRobot.ForwardKinematics(q).block<3,1>(0,3);
        SegmentTimes(i) = (Points.col(i) - Points.col(i - 1)).norm() / maxVel;
    }
    Eigen::MatrixXd dataToSave(numberOfSegments + 1, 4);
    dataToSave.col(0) = SegmentTimes.transpose();
    dataToSave.block(0, 1, numberOfSegments + 1, 3) = Points.transpose();
    SaveEE("../results/Points.txt", dataToSave);
	double acc = 0.5;
	double blend = 0.0;
	LWRRobot.SetAccelerationDecelerationAndBlendingPeriod(acc, acc, blend);
	
	//~ Initialize the time stamps
	double dt = 0.005;
	double StartTime = 0.0;
	double EndTime = SegmentTimes(1);
	for (int k = 2; k < SegmentTimes.cols(); ++k)
	{
		EndTime += (1.0 - blend) * SegmentTimes(k);
	}
	
	//~ Set the method for inverting the Jacobian matrix; set the subtask; set the error gain for CLIK
	//~ LWRRobot.SetPseudoInverse(RedundantRobot::SVFmod, 1.5, 0.07);
	//~ LWRRobot.SetPseudoInverse(RedundantRobot::SVF, 0.1, 10.0);
	//~ LWRRobot.SetPseudoInverse(RedundantRobot::JD, 0.04, 0.07);
	LWRRobot.SetPseudoInverse(RedundantRobot::JP);
	LWRRobot.SetSubtask(RedundantRobot::None);
	LWRRobot.SetErrorGain(100.0);
	
	//~ Set the integration method
	LWRRobot.SetIntegrationMethod(RedundantRobot::EM);
	
	//~ Matrices to store the results
	Eigen::MatrixXd TotalResults;
	Eigen::VectorXd Time;
	Eigen::MatrixXd Q;
	Eigen::MatrixXd DQ;
	Eigen::MatrixXd DDQ;
	Eigen::MatrixXd TRQ;
	Eigen::MatrixXd DesiredEEPos;
	Eigen::MatrixXd DesiredEEVel;
	Eigen::MatrixXd EEPos;
	Eigen::MatrixXd PosError;
	Eigen::VectorXd WallClockTime;
	
	//~ Solve the inverse kinematics
	TotalResults = LWRRobot.SolveInverseKinematics(q0, dq0, Points, SegmentTimes, StartTime, EndTime, dt);
	int steps = TotalResults.rows();
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	DesiredEEPos = TotalResults.block(0, 1 + 3 * DOF, steps, 3);
	DesiredEEVel = TotalResults.block(0, 4 + 3 * DOF, steps, 3);
	WallClockTime = TotalResults.block(0, 7 + 3 * DOF, steps, 1);
	
	std::cout << "Nominal motion time: " << EndTime << " seconds.\n";
	
	EEPos = Eigen::MatrixXd::Zero(steps,3);
	for (int k = 0; k < steps; ++k)
	{
		EEPos.row(k) = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3).transpose();
	}
	
	PosError = DesiredEEPos - EEPos;
	
	//~ Solve the inverse dynamics
	//~ TRQ = LWRRobot.SolveInverseDynamics(TotalResults.block(0, 1, steps, 3 * DOF));
	
	//~ Save the results to files
	SaveJnt("../results/q.txt", Q);
	SaveJnt("../results/dq.txt", DQ);
	SaveJnt("../results/ddq.txt", DDQ);
	//~ SaveJnt("../results/trq.txt", TRQ);
	
	SaveEE("../results/desPos.txt", DesiredEEPos);
	SaveEE("../results/desVel.txt", DesiredEEVel);
	SaveEE("../results/pos.txt", EEPos);
	SaveEE("../results/posError.txt", PosError);
	SaveEE("../results/time.txt", Time);
	SaveEE("../results/WallClockTime.txt", WallClockTime);
	
	return 0;
}
