Q = dlmread("results/q.txt");
Q = Q * 180/pi;

dQ = dlmread("results/dq.txt");
dQ = dQ * 180/pi;

ddQ = dlmread("results/ddq.txt");
ddQ = ddQ * 180/pi;

desPos = dlmread("results/desPos.txt");
desVel = dlmread("results/desVel.txt");

Pos = dlmread("results/pos.txt");

Error = dlmread("results/posError.txt");

t = dlmread("results/time.txt");

wclock = dlmread("results/WallClockTime.txt");

Q1357_max = ones(rows(Q), 1) * 165;
Q1357_min = -Q1357_max;
Q246_max = ones(rows(Q), 1) * 115;
Q246_min = -Q246_max;
dQdt_max = ones(rows(dQ), 1) * 150;
dQdt_min = -dQdt_max;
d2Qdt2_max = ones(rows(ddQ), 1) * 350;
d2Qdt2_min = -d2Qdt2_max;

%i=3;
%plot(t,desPos(:,i),t,Pos(:,i)); legend("des", "msr"); xlabel("t, s"); ylabel("pos, m"); grid on;

%plot(t,Q(:,i));
%plot(t,dQ(:,i));
%plot(t,ddQ(:,i));
plot(t,Q, t, Q1357_max, "r", t, Q1357_min, "r", t, Q246_max, "r", t, Q246_min, "r"); legend("1", "2", "3", "4", "5", "6", "7"); xlabel("t, s"); ylabel("q, deg"); grid on;
%plot(t,dQ, t, dQdt_max, "r", t, dQdt_min, "r"); legend("1", "2", "3", "4", "5", "6", "7"); xlabel("t, s"); ylabel("dq, deg/s"); grid on;
%plot(t,ddQ, t, d2Qdt2_max, "r", t, d2Qdt2_min, "r"); legend("1", "2", "3", "4", "5", "6", "7"); xlabel("t, s"); ylabel("ddq, deg/s^2"); grid on;
%plot(t,Error); legend("x", "y", "z"); xlabel("t, s"); ylabel("error, m"); grid on;
%plot(t,desPos); legend("x", "y", "z"); xlabel("t, s"); ylabel("pos, m"); grid on;
%plot3(desPos(:,1),desPos(:,2),desPos(:,3)); xlabel("x, m"); ylabel("y, m"); zlabel("z, m"); grid on;
%plot(t,desVel); legend("x", "y", "z"); xlabel("t, s"); ylabel("vel, m/s"); grid on;
%plot(t,wclock); xlabel("t, s"); ylabel("Wall clock time, microsec"); grid on;

E = abs(Error);
Enorm = zeros(length(E), 1);
for i = 1:length(E)
	Enorm(i, 1) = norm(E(i, :));
end
Emean = mean(Enorm);
Emax = max(Enorm);