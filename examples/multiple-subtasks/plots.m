function plots(i)

Q = dlmread("results/q.txt");
Q = Q * 180/pi;

dQ = dlmread("results/dq.txt");
dQ = dQ * 180/pi;

ddQ = dlmread("results/ddq.txt");
ddQ = ddQ * 180/pi;

desPos = dlmread("results/desPos.txt");

Pos = dlmread("results/pos.txt");

Error = dlmread("results/posError.txt");

ObstacleDistance = dlmread("results/ObstacleDistance.txt");

WallClockTime = dlmread("results/WallClockTime.txt");

Q2 = dlmread("results/q2.txt");
Q2 = Q2 * 180/pi;

dQ2 = dlmread("results/dq2.txt");
dQ2 = dQ2 * 180/pi;

ddQ2 = dlmread("results/ddq2.txt");
ddQ2 = ddQ2 * 180/pi;

Pos2 = dlmread("results/pos2.txt");

Error2 = dlmread("results/posError2.txt");

ObstacleDistance2 = dlmread("results/ObstacleDistance2.txt");

WallClockTime2 = dlmread("results/WallClockTime2.txt");

Q3 = dlmread("results/q3.txt");
Q3 = Q3 * 180/pi;

dQ3 = dlmread("results/dq3.txt");
dQ3 = dQ3 * 180/pi;

ddQ3 = dlmread("results/ddq3.txt");
ddQ3 = ddQ3 * 180/pi;

Pos3 = dlmread("results/pos3.txt");

Error3 = dlmread("results/posError3.txt");

ObstacleDistance3 = dlmread("results/ObstacleDistance3.txt");

WallClockTime3 = dlmread("results/WallClockTime3.txt");

t = dlmread("results/time.txt");

%plot(t,desPos,t,Pos);

%plot(t,Q(:,i),t,Q2(:,i),t,Q3(:,i)); legend("obstacle", "obstacle-SR", "none"); grid on;
%plot(t,dQ(:,i),t,dQ2(:,i),t,dQ3(:,i)); legend("obstacle", "obstacle-SR", "none"); grid on;
%plot(t,ddQ(:,i),t,ddQ2(:,i),t,ddQ3(:,i)); legend("obstacle", "obstacle-SR", "none"); grid on;
plot(t,Q); legend("1", "2", "3", "4", "5", "6", "7"); xlabel("t, s"); ylabel("joint pos, deg"); grid on;
%plot(t,dQ); legend("1", "2", "3", "4", "5", "6", "7"); xlabel("t, s"); ylabel("joint vel, deg/s"); grid on;
%plot(t,ddQ); legend("1", "2", "3", "4", "5", "6", "7"); xlabel("t, s"); ylabel("joint acc, deg/s^2"); grid on;
%plot(t,Error); legend("x", "y", "z"); grid on; xlabel("t, s"); ylabel("error, m");
%plot(t,ObstacleDistance);
%plot3(desPos(:,1), desPos(:,2), desPos(:,3)); xlabel("x, m"); ylabel("y, m"); zlabel("z, m");
%plot(t,ObstacleDistance,t,ObstacleDistance2,t,ObstacleDistance3); legend("obstacle", "obstacle-SR", "none"); xlabel("t,s"); ylabel("Obstacle distance, m"); grid on;
%plot(t,WallClockTime,t,WallClockTime2,t,WallClockTime3); legend("obstacle", "obstacle-SR", "none"); xlabel("t, s"); ylabel("Wall clock time, microsec"); grid on;
end