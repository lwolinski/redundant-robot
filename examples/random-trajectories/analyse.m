function Results = analyse()

filename = "results/Table-dt002";
Results{1} = ReadAndAnalyseData(filename);

filename = "results/Table-dt01";
Results{2} = ReadAndAnalyseData(filename);

filename = "results/Table-dt05";
Results{3} = ReadAndAnalyseData(filename);

filename = "results/Table-dt1";
Results{4} = ReadAndAnalyseData(filename);

end

function IntResults = ReadAndAnalyseData(filename)

IntegrationMethods = {"-EE", "-ET", "-RK4"};
InverseMethods = {"-JP", "-JD", "-SVF", "-SVFmod", "-JT"};

%IntResults;
i = 1;
for IntMethod = IntegrationMethods
    IntMethodFilename = strcat(filename, IntMethod);
    
%    InvResults = zeros(size(InverseMethods), 13);
    InvResults = zeros(size(InverseMethods), 4);
    k = 1;
    for InvMethod = InverseMethods
        LocalFilename = strcat(IntMethodFilename, strcat(InvMethod, ".txt"));
        
        Table = dlmread(LocalFilename{1});
        
        Error = Table(:, 1);
        TotalTime = Table(:, 2) / 1000; % convert microseconds to miliseconds
        IterationTime = Table(:, 3) / 1000; % convert microseconds to miliseconds
        PercentJL = ( 1 - sum(Table(:, 4)) / rows(Table) ) * 100;
        
%        InvResults(k, 1:4) = stats(Error);
%        InvResults(k, 5:8) = stats(TotalTime);
%        InvResults(k, 9:12) = stats(IterationTime);
%        InvResults(k, 13) = PercentJL;
        
        InvResults(k, 1) = mean(Error);
        InvResults(k, 2) = mean(TotalTime);
        InvResults(k, 3) = mean(IterationTime);
        InvResults(k, 4) = PercentJL;
        
        k = k + 1;
    end
    
    IntResults{i} = InvResults;
    i = i + 1;
end

end

function [Mean, StDev, Max, Min] = stats(x)

    Mean = mean(x);
    StDev = std(x);
    Min = min(x);
    Max = max(x);

end