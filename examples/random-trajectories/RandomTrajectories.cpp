//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "RedundantRobot/RobotModel.h"
#include <iostream>
#include <fstream>
#include <string>

void SaveJnt(const std::string &OutputFileName, const Eigen::MatrixXd &Jnt)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << Jnt;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
}

void SaveResults(const std::string &OutputFileName, const Eigen::MatrixXd &Results)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << Results;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
}

Eigen::MatrixXd ReadPoints(const std::string& filename)
{
	//~ Each row is a pair of points in the 7-DOF manipulator's joint space, so 14 elements in each row.
	
	const int MaxBufferSize = 20000;
	
	std::ifstream FileToRead;

	FileToRead.open(filename);

	if(!FileToRead)
	{
		std::cout << "Could not open a file." << std::endl;
	}
	
	//~ Read the file into the buffer
	double TempBuffer[MaxBufferSize];
	int k = 0;
	while(!FileToRead.eof())
	{
		FileToRead >> TempBuffer[k];
		k++;
	}
	
	//~ Read the positions from the buffer into the (k / 14) * 14 matrix
	Eigen::MatrixXd JointPoints( k/14, 14 );
	for (int i = 0; i < (k/14); ++i)
	{
		for (int j = 0; j < 14; ++j)
		{
			JointPoints(i, j) = TempBuffer[14 * i + j];
		}
	}
	
	return JointPoints;
}

Eigen::RowVectorXd PseudoInverseTypeSimulation( RedundantRobot::RobotModel& LWRRobot, const Eigen::VectorXd& JointPosStart, const Eigen::VectorXd& JointPosEnd, const Eigen::VectorXd& JointLim, double dt, const int DOF )
{
	//~ Get the initial and final EE position
	Eigen::Vector3d StartEEPos = LWRRobot.ForwardKinematics(JointPosStart).block<3,1>(0,3);
	Eigen::Vector3d EndEEPos = LWRRobot.ForwardKinematics(JointPosEnd).block<3,1>(0,3);
	
	double MaxVel = 1.0;
	double TrajectoryLength = (EndEEPos - StartEEPos).norm();
	double AccelerationPeriod = 0.5;
	double DecelerationPeriod = 0.5;
	LWRRobot.SetAccelerationAndDecelerationPeriod(AccelerationPeriod, DecelerationPeriod);
	
	//~ Initialize the time stamps
	double StartTime = 0.0;
	double EndTime = 2.0 * TrajectoryLength / ((2.0 - AccelerationPeriod - DecelerationPeriod) * MaxVel);
	
	//~ Prepare the data for the trajectory generator
	Eigen::RowVectorXd SegmentTimes(2);
	SegmentTimes(0) = StartTime;
	SegmentTimes(1) = EndTime;
	Eigen::MatrixXd Points(3,2);
	Points.col(0) = StartEEPos;
	Points.col(1) = EndEEPos;
	
	//~ Matrices to store the results
	Eigen::MatrixXd TotalResults;
	Eigen::VectorXd Time;
	Eigen::MatrixXd Q;
	Eigen::MatrixXd DQ;
	Eigen::MatrixXd DDQ;
	Eigen::MatrixXd DesiredEEPos;
	Eigen::MatrixXd DesiredEEVel;
	Eigen::VectorXd WallClockTime;
	
	Eigen::VectorXd JointVelStart = Eigen::VectorXd::Zero(DOF);
	
	//~ Solve the inverse kinematics
	TotalResults = LWRRobot.SolveInverseKinematics(JointPosStart, JointVelStart, Points, SegmentTimes, StartTime, EndTime, dt);
	int steps = TotalResults.rows();
	Time = TotalResults.col(0);
	Q = TotalResults.block(0, 1, steps, DOF);
	DQ = TotalResults.block(0, 1 + DOF, steps, DOF);
	DDQ = TotalResults.block(0, 1 + 2 * DOF, steps, DOF);
	DesiredEEPos = TotalResults.block(0, 1 + 3 * DOF, steps, 3);
	DesiredEEVel = TotalResults.block(0, 4 + 3 * DOF, steps, 3);
	WallClockTime = TotalResults.block(0, 7 + 3 * DOF, steps, 1);
	
	Eigen::MatrixXd EEPos(steps, 3);
	Eigen::MatrixXd PosError(steps, 3);
	Eigen::VectorXd PosErrorSquared(steps);
	for (int k = 0; k < steps; ++k)
	{
		Eigen::Vector3d Pos = LWRRobot.ForwardKinematics(Q.row(k).transpose()).block<3,1>(0,3);
		EEPos.row(k) = Pos.transpose();
		Eigen::Vector3d Error = DesiredEEPos.row(k).transpose() - Pos;
		PosError.row(k) = Error.transpose();
		PosErrorSquared(k) = Error.transpose() * Error;
	}
	
	double TotalWallClockTime = 0.0;
	double PosErrorSquaredIntegrated = 0.0;
	int JointLimBroken = 0;
	for (int k = 0; k < steps - 1; ++k)
	{
		TotalWallClockTime += WallClockTime(k);
		PosErrorSquaredIntegrated += 0.5 * dt * ( PosErrorSquared(k) + PosErrorSquared(k + 1) );
		
		for (int j = 0; j < DOF; ++j)
		{
			if ( abs(Q(k,j)) > (JointLim(j) * M_PI / 180.0) )
			{
				JointLimBroken = 1;
			}
		}
	}
	
	double MeanStepWallClockTime = TotalWallClockTime / steps;
	
	Eigen::RowVectorXd Results(4);
	Results(0) = PosErrorSquaredIntegrated;
	Results(1) = TotalWallClockTime;
	Results(2) = MeanStepWallClockTime;
	Results(3) = JointLimBroken;
	
	return Results;
}

void IntegrationTypeSimulation( std::string FileName, RedundantRobot::RobotModel& LWRRobot, const Eigen::MatrixXd& JointPoints, const Eigen::VectorXd& JointLim, double dt, const int DOF )
{
	//~ int NumberOfPoints = JointPoints.rows();
	int NumberOfPoints = 10;
	Eigen::MatrixXd Results(NumberOfPoints, 4);
	
	//~ JP
	std::cout << "Jacobian pseudoinverse (JP)" << std::endl;
	LWRRobot.SetPseudoInverse(RedundantRobot::JP);
	
	for (int k = 0; k < NumberOfPoints; ++k)
	{
		std::cout << "Step: " << k << " / " << NumberOfPoints - 1 << std::endl;
		//~ Initialize the joint positions
		Eigen::VectorXd JointPosStart = JointPoints.block(k, 0, 1, DOF).transpose() * (M_PI / 180.0);
		Eigen::VectorXd JointPosEnd = JointPoints.block(k, DOF, 1, DOF).transpose() * (M_PI / 180.0);
		
		Results.row(k) = PseudoInverseTypeSimulation( LWRRobot, JointPosStart, JointPosEnd, JointLim, dt, DOF );
	}
	
	SaveResults(FileName+"-JP.txt", Results);
	
	//~ JD
	std::cout << "Jacobian damping (JD)" << std::endl;
	LWRRobot.SetPseudoInverse(RedundantRobot::JD, 0.04, 0.001);
	
	for (int k = 0; k < NumberOfPoints; ++k)
	{
		std::cout << "Step: " << k << " / " << NumberOfPoints - 1 << std::endl;
		//~ Initialize the joint positions
		Eigen::VectorXd JointPosStart = JointPoints.block(k, 0, 1, DOF).transpose() * (M_PI / 180.0);
		Eigen::VectorXd JointPosEnd = JointPoints.block(k, DOF, 1, DOF).transpose() * (M_PI / 180.0);
		
		Results.row(k) = PseudoInverseTypeSimulation( LWRRobot, JointPosStart, JointPosEnd, JointLim, dt, DOF );
	}
	
	SaveResults(FileName+"-JD.txt", Results);
	
	//~ SVF
	std::cout << "Singular value filtering (SVF)" << std::endl;
	LWRRobot.SetPseudoInverse(RedundantRobot::SVF, 0.005, 10.0);
	
	for (int k = 0; k < NumberOfPoints; ++k)
	{
		std::cout << "Step: " << k << " / " << NumberOfPoints - 1 << std::endl;
		//~ Initialize the joint positions
		Eigen::VectorXd JointPosStart = JointPoints.block(k, 0, 1, DOF).transpose() * (M_PI / 180.0);
		Eigen::VectorXd JointPosEnd = JointPoints.block(k, DOF, 1, DOF).transpose() * (M_PI / 180.0);
		
		Results.row(k) = PseudoInverseTypeSimulation( LWRRobot, JointPosStart, JointPosEnd, JointLim, dt, DOF );
	}
	
	SaveResults(FileName+"-SVF.txt", Results);
	
	//~ SVF modified
	std::cout << "Singular value filtering modified (SVFmod)" << std::endl;
	LWRRobot.SetPseudoInverse(RedundantRobot::SVFmod, 0.005, 0.001);
	
	for (int k = 0; k < NumberOfPoints; ++k)
	{
		std::cout << "Step: " << k << " / " << NumberOfPoints - 1 << std::endl;
		//~ Initialize the joint positions
		Eigen::VectorXd JointPosStart = JointPoints.block(k, 0, 1, DOF).transpose() * (M_PI / 180.0);
		Eigen::VectorXd JointPosEnd = JointPoints.block(k, DOF, 1, DOF).transpose() * (M_PI / 180.0);
		
		Results.row(k) = PseudoInverseTypeSimulation( LWRRobot, JointPosStart, JointPosEnd, JointLim, dt, DOF );
	}
	
	SaveResults(FileName+"-SVFmod.txt", Results);
	
	//~ JT
	std::cout << "Jacobian transpose (JT)" << std::endl;
	LWRRobot.SetPseudoInverse(RedundantRobot::JT);
	
	for (int k = 0; k < NumberOfPoints; ++k)
	{
		std::cout << "Step: " << k << " / " << NumberOfPoints - 1 << std::endl;
		//~ Initialize the joint positions
		Eigen::VectorXd JointPosStart = JointPoints.block(k, 0, 1, DOF).transpose() * (M_PI / 180.0);
		Eigen::VectorXd JointPosEnd = JointPoints.block(k, DOF, 1, DOF).transpose() * (M_PI / 180.0);
		
		Results.row(k) = PseudoInverseTypeSimulation( LWRRobot, JointPosStart, JointPosEnd, JointLim, dt, DOF );
	}
	
	SaveResults(FileName+"-JT.txt", Results);
}

void Simulation( std::string FileName, RedundantRobot::RobotModel& LWRRobot, const Eigen::MatrixXd& JointPoints, const Eigen::VectorXd& JointLim, double dt, const int DOF )
{
	RedundantRobot::integrationType IntegrationMethods[] = {RedundantRobot::EE, RedundantRobot::ET, RedundantRobot::RK4};
	
	for (auto method : IntegrationMethods )
	{
		std::string LocalFileName = FileName;
		LWRRobot.SetIntegrationMethod(method);
		switch (method)
		{
			case RedundantRobot::EE:
			{
				LocalFileName += "-EE";
				std::cout << "Explicit Euler" << std::endl;
			}
				break;
				
			case RedundantRobot::ET:
			{
				LocalFileName += "-ET";
				std::cout << "Explicit Trapezoid" << std::endl;
			}
				break;
				
			case RedundantRobot::RK4:
			{
				LocalFileName += "-RK4";
				std::cout << "Runge-Kutta 4th order" << std::endl;
			}
				break;
		}
		
		IntegrationTypeSimulation( LocalFileName, LWRRobot, JointPoints, JointLim, dt, DOF );
	}

}

int main()
{
	//~ Read the joint space points from file
	Eigen::MatrixXd JointPoints = ReadPoints("../Points.txt");
	
	const int DOF = 7;
	
	//~ Create a robot with given EE
	Eigen::Vector3d angles_EE(0.0, 0.0, 0.0);
	Eigen::Vector3d r_EE(0.1, 0.0, 0.078);
	RedundantRobot::RobotModel LWRRobot( angles_EE, r_EE );
	
	//~ Set the joint limits
	Eigen::VectorXd JointLim(DOF);
	JointLim << 170.0, 120.0, 170.0, 120.0, 170.0, 120.0, 170.0;
	Eigen::VectorXd JointMax = JointLim * (M_PI / 180.0);
	Eigen::VectorXd JointMin = -JointLim * (M_PI / 180.0);
	LWRRobot.SetJointLimits(JointMax, JointMin);
	
	//~ LWRRobot.SetSubtask(RedundantRobot::JointLimit);
	//~ LWRRobot.SetSubtaskConstraintGain(0.2);
	LWRRobot.SetSubtask(RedundantRobot::None);
	
	//~ Name of the file to save the results
	std::string FileName;
	
	//~ Initialize the time step of the solution
	double dt = 0.002;
	FileName = "../results/Table-dt002";
	std::cout << "Time step: " << dt << std::endl;
	
	LWRRobot.SetErrorGain(1.0/dt);
	
	Simulation( FileName, LWRRobot, JointPoints, JointLim, dt, DOF );
	
	//~ Another time step
	dt = 0.01;
	FileName = "../results/Table-dt01";
	std::cout << "Time step: " << dt << std::endl;
	
	LWRRobot.SetErrorGain(1.0/dt);
	
	Simulation( FileName, LWRRobot, JointPoints, JointLim, dt, DOF );
	
	//~ Another time step
	dt = 0.05;
	FileName = "../results/Table-dt05";
	std::cout << "Time step: " << dt << std::endl;
	
	LWRRobot.SetErrorGain(1.0/dt);
	
	Simulation( FileName, LWRRobot, JointPoints, JointLim, dt, DOF );
	
	//~ Another time step
	dt = 0.1;
	FileName = "../results/Table-dt1";
	std::cout << "Time step: " << dt << std::endl;
	
	LWRRobot.SetErrorGain(1.0/dt);
	
	Simulation( FileName, LWRRobot, JointPoints, JointLim, dt, DOF );
	
	return 0;
}
