//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "RedundantRobot/DynamicModelMDH.h"

namespace RedundantRobot
{

DynamicModelMDH::DynamicModelMDH( int DOF, const Eigen::MatrixXd &AllRotAxes, const Eigen::VectorXd &Masses, const Eigen::MatrixXd &AllFirstMomentsOfMass, const Eigen::MatrixXd &AllInertias, const Eigen::Vector3d &Gravity, const Eigen::MatrixXd &MDHangles, const Eigen::VectorXd &MDHlengthsX, const Eigen::VectorXd &MDHlengthsZ )
: DynamicModel( DOF, AllRotAxes, Masses, AllFirstMomentsOfMass, AllInertias, Gravity ), MDHangles(MDHangles), MDHlengthsX(MDHlengthsX), MDHlengthsZ(MDHlengthsZ)
{
}

DynamicModelMDH::DynamicModelMDH( const DynamicModelMDH &other )
: DynamicModel(other), MDHangles(other.MDHangles), MDHlengthsX(other.MDHlengthsX), MDHlengthsZ(other.MDHlengthsZ)
{
}

DynamicModelMDH& DynamicModelMDH::operator= ( const DynamicModelMDH &other )
{
	if (&other == this)
	{
		return *this;
	}
	
	DynamicModel::operator= (other);
	
	MDHangles = other.MDHangles;
	MDHlengthsX = other.MDHlengthsX;
	MDHlengthsZ = other.MDHlengthsZ;
	
	return *this;
}

DynamicModelMDH::~DynamicModelMDH()
{
}

Eigen::MatrixXd DynamicModelMDH::TransformationMatrix( const Eigen::VectorXd &JointPos, int j )
{
//	Computes the transformation matrix from the previous link frame to the current link frame.

	Eigen::Matrix4d A_ij;

	A_ij << cos( JointPos(j) ), -sin( JointPos(j) ), 0.0, MDHlengthsX(j),
			sin( JointPos(j) ) * cos( MDHangles(j) ), cos( JointPos(j) ) * cos( MDHangles(j) ), -sin( MDHangles(j) ), -MDHlengthsZ(j) * sin( MDHangles(j) ),
			sin( JointPos(j) ) * sin( MDHangles(j) ), cos( JointPos(j) ) * sin( MDHangles(j) ), cos( MDHangles(j) ), MDHlengthsZ(j) * cos( MDHangles(j) ),
			0.0, 0.0, 0.0, 1.0;

	return A_ij;
}

} //namespace RedundantRobot
