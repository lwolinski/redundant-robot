//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "RedundantRobot/KinematicModelMDH.h"

//~ using RedundantRobot::KinematicModelMDH;

namespace RedundantRobot
{

KinematicModelMDH::KinematicModelMDH( int DOF, const Eigen::VectorXd &MDHangles, const Eigen::VectorXd &MDHlengthsX, const Eigen::VectorXd &MDHlengthsZ )
: KinematicModel(DOF), MDHangles(MDHangles), MDHlengthsX(MDHlengthsX), MDHlengthsZ(MDHlengthsZ)
{
	A_EE = Eigen::MatrixXd::Identity(4,4);
}

KinematicModelMDH::KinematicModelMDH( int DOF, const Eigen::VectorXd &MDHangles, const Eigen::VectorXd &MDHlengthsX, const Eigen::VectorXd &MDHlengthsZ, const Eigen::Vector3d &angles_EE, const Eigen::Vector3d &r_EE )
: KinematicModel(DOF), MDHangles(MDHangles), MDHlengthsX(MDHlengthsX), MDHlengthsZ(MDHlengthsZ)
{
	A_EE = EEFrame(angles_EE, r_EE);
}

KinematicModelMDH::KinematicModelMDH( const KinematicModelMDH &other )
: KinematicModel(other), MDHangles(other.MDHangles), MDHlengthsX(other.MDHlengthsX), MDHlengthsZ(other.MDHlengthsZ), A_EE(other.A_EE)
{
}

KinematicModelMDH& KinematicModelMDH::operator= ( const KinematicModelMDH &other )
{
	if (&other == this) // self-assignment
	{
		return *this;
	}
	
	KinematicModel::operator= (other);
	
	MDHangles = other.MDHangles;
	MDHlengthsX = other.MDHlengthsX;
	MDHlengthsZ = other.MDHlengthsZ;
	A_EE = other.A_EE;
	
	return *this;
}

KinematicModelMDH::~KinematicModelMDH()
{
}

Eigen::MatrixXd KinematicModelMDH::ForwardKinematics( const Eigen::VectorXd &JointPos )
{
//	Solves the forward kinematics problem as a transformation matrix from the base frame to the end effector frame.
//	A_j after the loop is the transformation matrix from the base frame to the last link frame,
//	A_EE is the transformation matrix from the last link to the end effector frame,
//	A_j * A_EE is the transformation matrix from the base frame to the end effector frame.

	Eigen::Matrix4d A_i = Eigen::Matrix4d::Identity();

	Eigen::Matrix4d A_ij, A_j;

	for (int j = 0; j < DOF; j++)
	{
		A_ij << cos( JointPos(j) ), -sin( JointPos(j) ), 0.0, MDHlengthsX(j),
				sin( JointPos(j) ) * cos( MDHangles(j) ), cos( JointPos(j) ) * cos( MDHangles(j) ), -sin( MDHangles(j) ), -MDHlengthsZ(j) * sin( MDHangles(j) ),
				sin( JointPos(j) ) * sin( MDHangles(j) ), cos( JointPos(j) ) * sin( MDHangles(j) ), cos( MDHangles(j) ), MDHlengthsZ(j) * cos( MDHangles(j) ),
				0.0, 0.0, 0.0, 1.0;

		A_j = A_i * A_ij;
		A_i = A_j;
	}

	return A_j * A_EE;
}

Eigen::MatrixXd KinematicModelMDH::GetJacobian( const Eigen::VectorXd &JointPos )
{
//	Computes the manipulator's Jacobian J (size 3 * DOF).

	Eigen::Matrix4d A_i = Eigen::Matrix4d::Identity();

	Eigen::Matrix4d A_ij, A_j;

	Eigen::MatrixXd z(3, DOF), p(3, DOF);

	for (int j = 0; j < DOF; j++)
	{
		p.col(j) = A_i.block<3,1>(0,3);

		A_ij << cos( JointPos(j) ), -sin( JointPos(j) ), 0.0, MDHlengthsX(j),
				sin( JointPos(j) ) * cos( MDHangles(j) ), cos( JointPos(j) ) * cos( MDHangles(j) ), -sin( MDHangles(j) ), -MDHlengthsZ(j) * sin( MDHangles(j) ),
				sin( JointPos(j) ) * sin( MDHangles(j) ), cos( JointPos(j) ) * sin( MDHangles(j) ), cos( MDHangles(j) ), MDHlengthsZ(j) * cos( MDHangles(j) ),
				0.0, 0.0, 0.0, 1.0;

		A_j = A_i * A_ij;
		A_i = A_j;
		z.col(j) = A_i.block<3,3>(0,0) * Eigen::Vector3d(0.0, 0.0, 1.0);
	}

	Eigen::Vector3d p_EE = (A_j * A_EE).block<3,1>(0,3);

	Eigen::MatrixXd J(3,DOF);

	for (int j = 0; j < DOF; j++)
	{
		J.col(j) = Eigen::Vector3d(z.col(j)).cross( p_EE - p.col(j) );
	}

	return J;
}

//~ Eigen::VectorXd KinematicModelMDH::GetEndEffectorVel( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &NextEndEffectorPos, double StepSize )
//~ {
	//~ Get the end effector velocity as the difference between the desired position in the next step and the current position, divided by the step size
	//~ return ( NextEndEffectorPos - ForwardKinematics( JointPos ).block<3,1>(0,3) ) / StepSize;
//~ }

Eigen::VectorXd KinematicModelMDH::GetEndEffectorError( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &EndEffectorPos )
{
//	End effector position error for the CLIK
	//~ Eigen::MatrixXd ActualA_EE = ForwardKinematics( JointPos );
	//~ return EndEffectorPos - ActualA_EE.block<3,1>(0,3);
	//~ Eigen::VectorXd ActualEndEffectorPos = ForwardKinematics( JointPos ).block<3,1>(0,3);
	//~ return EndEffectorPos - ActualEndEffectorPos;
	return EndEffectorPos - ForwardKinematics( JointPos ).block<3,1>(0,3);
}

Eigen::MatrixXd KinematicModelMDH::GetObstacleJacobian( const Eigen::VectorXd &JointPos )
{
//	Computes the Jacobian of the part of the manipulator doing the obstacle avoidance task (size 3 * DOF).

	Eigen::Matrix4d A_i = Eigen::Matrix4d::Identity();

	Eigen::Matrix4d A_ij, A_j;

	Eigen::MatrixXd z(3, DOF), p(3, DOF);

	int ObstacleAvoidanceDOF = 2; // we will use the number of joints equal to the ObstacleAvoidanceDOF to avoid the obstacle
	for (int j = 0; j < ObstacleAvoidanceDOF; j++)
	{
		p.col(j) = A_i.block<3,1>(0,3);

		A_ij << cos( JointPos(j) ), -sin( JointPos(j) ), 0.0, MDHlengthsX(j),
				sin( JointPos(j) ) * cos( MDHangles(j) ), cos( JointPos(j) ) * cos( MDHangles(j) ), -sin( MDHangles(j) ), -MDHlengthsZ(j) * sin( MDHangles(j) ),
				sin( JointPos(j) ) * sin( MDHangles(j) ), cos( JointPos(j) ) * sin( MDHangles(j) ), cos( MDHangles(j) ), MDHlengthsZ(j) * cos( MDHangles(j) ),
				0.0, 0.0, 0.0, 1.0;

		A_j = A_i * A_ij;
		A_i = A_j;
		z.col(j) = A_i.block<3,3>(0,0) * Eigen::Vector3d(0.0, 0.0, 1.0);
	}

	//~ The vector from the beginning of the ObstacleAvoidanceDOF-th link frame locating the critical point
	Eigen::Matrix4d A_ObstacleAvoidance = Eigen::Matrix4d::Identity();
	Eigen::Vector3d ObstacleAvoidancePoint(0.0, MDHlengthsZ(2), 0.0);
	A_ObstacleAvoidance.block<3,1>(0,3) = ObstacleAvoidancePoint;
	
	Eigen::Vector3d p_EE = (A_j * A_ObstacleAvoidance).block<3,1>(0,3);

	Eigen::MatrixXd J = Eigen::MatrixXd::Zero(3,DOF);

	for (int j = 0; j < ObstacleAvoidanceDOF; j++)
	{
		J.col(j) = Eigen::Vector3d(z.col(j)).cross( p_EE - p.col(j) );
	}

	return J;
}

Eigen::Vector3d KinematicModelMDH::ObstacleKinematics( const Eigen::VectorXd &JointPos )
{
//~ Computes the location of the critical point (point avoiding the obstacle) on the robot

	Eigen::Matrix4d A_i = Eigen::Matrix4d::Identity();

	Eigen::Matrix4d A_ij, A_j;
	
	int ObstacleAvoidanceDOF = 2; // we will use the number of joints equal to the ObstacleAvoidanceDOF to avoid the obstacle
	for (int j = 0; j < ObstacleAvoidanceDOF; j++)
	{
		A_ij << cos( JointPos(j) ), -sin( JointPos(j) ), 0.0, MDHlengthsX(j),
				sin( JointPos(j) ) * cos( MDHangles(j) ), cos( JointPos(j) ) * cos( MDHangles(j) ), -sin( MDHangles(j) ), -MDHlengthsZ(j) * sin( MDHangles(j) ),
				sin( JointPos(j) ) * sin( MDHangles(j) ), cos( JointPos(j) ) * sin( MDHangles(j) ), cos( MDHangles(j) ), MDHlengthsZ(j) * cos( MDHangles(j) ),
				0.0, 0.0, 0.0, 1.0;

		A_j = A_i * A_ij;
		A_i = A_j;
	}

	//~ The vector from the beginning of the ObstacleAvoidanceDOF-th link frame locating the critical point
	Eigen::Matrix4d A_ObstacleAvoidance = Eigen::Matrix4d::Identity();
	Eigen::Vector3d ObstacleAvoidancePoint(0.0, MDHlengthsZ(2), 0.0);
	A_ObstacleAvoidance.block<3,1>(0,3) = ObstacleAvoidancePoint;
	
	return (A_j * A_ObstacleAvoidance).block<3,1>(0,3);
}

Eigen::MatrixXd KinematicModelMDH::EEFrame( const Eigen::Vector3d &angles_EE, const Eigen::Vector3d &r_EE )
{
//	Computes the transformation matrix from the last link frame to the end effector frame.
	Eigen::Matrix4d A_EE;

	A_EE << cos( angles_EE(2) ) * cos( angles_EE(0) ) - sin( angles_EE(2) ) * cos( angles_EE(1) ) * sin( angles_EE(0) ), -sin( angles_EE(2) ) * cos( angles_EE(0) ) - cos( angles_EE(2) ) * cos( angles_EE(1) ) * sin( angles_EE(0) ), sin( angles_EE(1) ) * sin( angles_EE(0) ), r_EE(0),
			sin( angles_EE(2) ) * cos( angles_EE(1) ) * cos( angles_EE(0) ) + cos( angles_EE(2) ) * sin( angles_EE(0) ), cos( angles_EE(2) ) * cos( angles_EE(1) ) * cos( angles_EE(0) ) - sin( angles_EE(2) ) * sin( angles_EE(0) ), -sin( angles_EE(1) ) * cos( angles_EE(0) ), r_EE(1),
			sin( angles_EE(2) ) * sin( angles_EE(1) ), cos( angles_EE(2) ) * sin( angles_EE(1) ), cos( angles_EE(1) ), r_EE(2),
			0.0, 0.0, 0.0, 1.0;

	return A_EE;
}

void KinematicModelMDH::SetNewTool( const Eigen::Vector3d &New_angles_EE, const Eigen::Vector3d &New_r_EE )
{
//	Allows to change the location and orientation of the end effector frame relative to the last link frame.
	A_EE = EEFrame( New_angles_EE, New_r_EE );
}

/*Eigen::Matrix3d Skew( const Eigen::Vector3d &Vector )
{
//	Returns a skew-symmetric matrix from a vector.
	Eigen::Matrix3d SkewSymmetricMatrix;

	SkewSymmetricMatrix << 0.0, -Vector(2), Vector(1),
			Vector(2), 0.0, -Vector(0),
			-Vector(1), Vector(0), 0.0;

	return SkewSymmetricMatrix;
}*/

} //namespace RedundantRobot
