//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

//~ The algorithm for computing the dynamic model of the manipulator is a modified version of: Ł. Woliński, P. Malczyk. Dynamic modeling and analysis of a lightweight robotic manipulator in joint space. Archive of Mechanical Engineering, August, 2015, Vol. 62, No. 2, pp. 279–302. 10.1023/A:1021909032551  

#include "RedundantRobot/DynamicModel.h"

namespace RedundantRobot
{

DynamicModel::DynamicModel( int DOF, const Eigen::MatrixXd &AllRotAxes, const Eigen::VectorXd &Masses, const Eigen::MatrixXd &AllFirstMomentsOfMass, const Eigen::MatrixXd &AllInertias, const Eigen::Vector3d &Gravity ) : DOF(DOF), AllRotAxes(AllRotAxes), Masses(Masses), AllFirstMomentsOfMass(AllFirstMomentsOfMass), AllInertias(AllInertias), Gravity(Gravity)
{
}

DynamicModel::DynamicModel( const DynamicModel &other )
: DOF(other.DOF), AllRotAxes(other.AllRotAxes), Masses(other.Masses), AllFirstMomentsOfMass(other.AllFirstMomentsOfMass), AllInertias(other.AllInertias), Gravity(other.Gravity)
{
}

DynamicModel& DynamicModel::operator= ( const DynamicModel &other )
{
	if (&other == this) // self-assignment
	{
		return *this;
	}
	
	DOF = other.DOF;
	AllRotAxes = other.AllRotAxes;
	Masses = other.Masses;
	AllFirstMomentsOfMass = other.AllFirstMomentsOfMass;
	AllInertias = other.AllInertias;
	Gravity = other.Gravity;
	
	return *this;
}

DynamicModel::~DynamicModel()
{
}

Eigen::MatrixXd DynamicModel::DynamicMatrices( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &JointVel )
{
	Eigen::MatrixXd H = Eigen::MatrixXd::Zero(6 * DOF, DOF);
	Eigen::MatrixXd dH = Eigen::MatrixXd::Zero(6 * DOF, DOF);
	Eigen::MatrixXd dB = Eigen::MatrixXd::Zero(6 * DOF, 6 * DOF);
	Eigen::MatrixXd Phi = Eigen::MatrixXd::Identity(6 * DOF, 6 * DOF);
	Eigen::MatrixXd Omega = Eigen::MatrixXd::Zero(6 * DOF, 6 * DOF);
	Eigen::MatrixXd MassMatrix = Eigen::MatrixXd::Zero(6 * DOF, 6 * DOF);
	Eigen::VectorXd GravForces = Eigen::VectorXd::Zero(6 * DOF);
	Eigen::VectorXd ExternalForces = Eigen::VectorXd::Zero(6 * DOF);
	
	Eigen::Matrix4d A_i = Eigen::Matrix4d::Identity();	// transformation matrix from the base frame to the i-th link frame (currently, before the loop, it's the transformation from the base to the base, i.e. identity)
	Eigen::Matrix4d A_j;	// transformation matrix from the base frame to the j-th link frame
	Eigen::Vector3d RotAxis;	// unit vector along rotational axis of the j-th joint expressed in the j-th link frame
	Eigen::Vector3d AngularVel = Eigen::Vector3d::Zero();	// angular velocity of the current link given in the base frame (currently, before the loop, it's the base angular velocity, so it equals to zero)
	
	for (int j = 0; j < DOF; j++)
	{
		Eigen::MatrixXd A_ij = TransformationMatrix( JointPos, j );
		A_j = A_i * A_ij;
		
		RotAxis = A_j.block<3,3>(0,0) * AllRotAxes.col(j);
		H.block<3,1>(6 * j + 3, j) = RotAxis;
		dH.block<3,1>(6 * j + 3, j) = skew(AngularVel) * RotAxis;
		
		if ( j > 0 )
		{
			int i = j - 1;
			
			Eigen::Vector3d r_ij = A_j.block<3,1>(0,3) - A_i.block<3,1>(0,3);	// vector linking the j-th and i-th link frame origins, expressed in the base frame
			
			Eigen::MatrixXd B_ij = Eigen::MatrixXd::Identity(6,6);
			B_ij.block<3,3>(0,3) = -skew(r_ij);
			dB.block<3,3>(6 * j, 6 * i + 3) = -skew(AngularVel) * skew(r_ij);
			Phi.block(6 * j, 0 , 6, 6 * (i + 1)) = B_ij * Phi.block(6 * i, 0, 6, 6 * (i + 1));
		}
		
		Eigen::Matrix3d Inertia = A_j.block<3,3>(0,0) * AllInertias.block<3,3>(0, 3 * j) * (A_j.block<3,3>(0,0)).transpose();
		Eigen::Vector3d FirstMomentOfMass = A_j.block<3,3>(0,0) * AllFirstMomentsOfMass.col(j);
		
		MassMatrix.block<3,3>(6 * j, 6 * j) = Masses(j) * Eigen::Matrix3d::Identity();
		MassMatrix.block<3,3>(6 * j, 6 * j + 3) = -skew(FirstMomentOfMass);
		MassMatrix.block<3,3>(6 * j + 3, 6 * j) = skew(FirstMomentOfMass);
		MassMatrix.block<3,3>(6 * j + 3, 6 * j + 3) = Inertia;
		
		GravForces.block<3,1>(6 * j, 0) = Masses(j) * Gravity;
		GravForces.block<3,1>(6 * j + 3, 0) = skew(FirstMomentOfMass) * Gravity;
		
		AngularVel += RotAxis * JointVel(j);
		
		Omega.block<3,3>(6 * j, 6 * j + 3) = -skew(AngularVel) * skew(FirstMomentOfMass);
		Omega.block<3,3>(6 * j + 3, 6 * j + 3) = skew(AngularVel) * Inertia;
		
		A_i = A_j;
	}
	
	//~ Manipulator inertia matrix
	Eigen::MatrixXd M = H.transpose() * Phi.transpose() * MassMatrix * Phi * H;
	
	//~ Manipulator Coriolis/centrifugal matrix
	Eigen::MatrixXd C = H.transpose() * Phi.transpose() * ( MassMatrix * Phi * ( dB * Phi * H + dH ) + Omega * Phi * H );
	
	//~ Torques produced by the gravity
	Eigen::VectorXd G = H.transpose() * Phi.transpose() * ( -GravForces );
	
	//~ Torques produced by the external forces
	Eigen::VectorXd Q = H.transpose() * Phi.transpose() * ( -ExternalForces );
	
	//~ Matrix to store all the dynamic matrices and to be returned
	Eigen::MatrixXd Matrices = Eigen::MatrixXd::Zero(DOF, 2 * DOF + 2);
	
	Matrices.block(0, 0, DOF, DOF) = M;
	Matrices.block(0, DOF, DOF, DOF) = C;
	Matrices.col(2 * DOF) = G;
	Matrices.col(2 * DOF + 1) = Q;

	return Matrices;
}

Eigen::VectorXd DynamicModel::InverseDynamicsStep( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &JointVel, const Eigen::VectorXd &JointAcc )
{
//	Computes one step of the inverse dynamics problem and returns joint torques.
//	Needs joint positions, velocities and accelerations from the current step.

	Eigen::MatrixXd Matrices = DynamicMatrices( JointPos, JointVel );
	Eigen::MatrixXd M = Matrices.block(0, 0, DOF, DOF);
	Eigen::MatrixXd C = Matrices.block(0, DOF, DOF, DOF);
	Eigen::VectorXd G = Matrices.col(2 * DOF);
	Eigen::VectorXd Q = Matrices.col(2 * DOF + 1);

//	Final results
	Eigen::VectorXd JointTrq = M * JointAcc + C * JointVel + G + Q;

	return JointTrq;
}

Eigen::Matrix3d DynamicModel::skew( const Eigen::Vector3d &Vector )
{
//	Returns a skew-symmetric matrix from a vector.
	Eigen::Matrix3d SkewSymmetricMatrix;

	SkewSymmetricMatrix << 0.0, -Vector(2), Vector(1),
			Vector(2), 0.0, -Vector(0),
			-Vector(1), Vector(0), 0.0;

	return SkewSymmetricMatrix;
}

} //namespace RedundantRobot
