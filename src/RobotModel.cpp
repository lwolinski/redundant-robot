//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "RedundantRobot/RobotModel.h"
#include "RedundantRobot/Trajectory.h"
#include <chrono>

namespace RedundantRobot
{

RobotModel::RobotModel() : DOF(7)
{
//	Modified Denavit-Hartenberg parameters:
	Eigen::VectorXd MDHangles(DOF); // angles
	MDHangles << 0.0, pi/2, -pi/2, -pi/2, pi/2, pi/2, -pi/2;
	Eigen::VectorXd MDHlengthsX(DOF); // lengths on X axis
	MDHlengthsX << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
	Eigen::VectorXd MDHlengthsZ(DOF); // lengths on Z axis
	MDHlengthsZ << 0.31, 0.0, 0.4, 0.0, 0.39, 0.0, 0.0;

	KinematicRobotModel = new KinematicModelMDH( DOF, MDHangles, MDHlengthsX, MDHlengthsZ );
	
	//~ Inertial parameters (source: A. Jubien, M. Gautier, A. Janot. Dynamic identification of the KUKA lightweight robot: Comparison between actual and confidential KUKA’s parameters. In: 2014 IEEE/ASME International Conference on Advanced Intelligent Mechatronics. Proceedings, July, 2014, pp. 483–488)
	Eigen::MatrixXd AllRotAxes = Eigen::MatrixXd::Zero(3, DOF);
	AllRotAxes.row(2) = Eigen::RowVectorXd::Ones(DOF);
	
	Eigen::VectorXd Masses = Eigen::VectorXd::Zero(DOF);
	
	Eigen::MatrixXd AllFirstMomentsOfMass(3, DOF);
	AllFirstMomentsOfMass.col(0) = Eigen::Vector3d::Zero();
	AllFirstMomentsOfMass.col(1) = Eigen::Vector3d( 1.35 * pow(10.0, -3.0), 3.46, 0.0 );
	AllFirstMomentsOfMass.col(2) = Eigen::Vector3d( 9.45 * pow(10.0, -4.0), -4.72 * pow(10.0, -4.0), 0.0 );
	AllFirstMomentsOfMass.col(3) = Eigen::Vector3d( -3.5 * pow(10.0, -3.0), -1.33, 0.0 );
	AllFirstMomentsOfMass.col(4) = Eigen::Vector3d( -6.56 * pow(10.0, -4.0), 4.07 * pow(10.0, -2.0), 0.0 );
	AllFirstMomentsOfMass.col(5) = Eigen::Vector3d( 8.35 * pow(10.0, -4.0), 2.86 * pow(10.0, -2.0), 0.0 );
	AllFirstMomentsOfMass.col(6) = Eigen::Vector3d( -2.98 * pow(10.0, -4.0), 9.54 * pow(10.0, -4.0), 0.0 );
	
	Eigen::MatrixXd AllInertias(3, 3 * DOF);
	AllInertias.block<3,3>(0,0) << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.15 * pow(10.0, -2.0);
	AllInertias.block<3,3>(0,3) << 1.25, 0.0, 0.0, 0.0, 0.0, -5.43 * pow(10.0, -4.0), 0.0, 5.43 * pow(10.0, -4.0), 1.25;
	AllInertias.block<3,3>(0,6) << 6.36 * pow(10.0, -3.0), 0.0, 0.0, 0.0, 0.0, 7.26 * pow(10.0, -4.0), 0.0, -7.26 * pow(10.0, -4.0), 1.08 * pow(10.0, -2.0);
	AllInertias.block<3,3>(0,9) << 0.413, 0.0, 0.0, 0.0, 0.0, 5.32 * pow(10.0, -4.0), 0.0, -5.32 * pow(10.0, -4.0), 0.418;
	AllInertias.block<3,3>(0,12) << 3.95 * pow(10.0, -3.0), 0.0, 0.0, 0.0, 0.0, 4.22 * pow(10.0, -4.0), 0.0, -4.22 * pow(10.0, -4.0), 6.33 * pow(10.0, -3.0);
	AllInertias.block<3,3>(0,15) << 1.17 * pow(10.0, -3.0), 0.0, 0.0, 0.0, 0.0, 1.02 * pow(10.0, -5.0), 0.0, -1.02 * pow(10.0, -5.0), 3.77 * pow(10.0, -3.0);
	AllInertias.block<3,3>(0,18) << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.2 * pow(10.0, -4.0);
	
	Eigen::Vector3d Gravity(0.0, 0.0, -9.80665);
	
	DynamicRobotModel = new DynamicModelMDH( DOF, AllRotAxes, Masses, AllFirstMomentsOfMass, AllInertias, Gravity, MDHangles, MDHlengthsX, MDHlengthsZ );
}

RobotModel::RobotModel( const Eigen::Vector3d &angles_EE, const Eigen::Vector3d &r_EE ) : DOF(7)
{
//	Modified Denavit-Hartenberg parameters:
	Eigen::VectorXd MDHangles(DOF); // angles
	MDHangles << 0.0, pi/2, -pi/2, -pi/2, pi/2, pi/2, -pi/2;
	Eigen::VectorXd MDHlengthsX(DOF); // lengths on X axis
	MDHlengthsX << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
	Eigen::VectorXd MDHlengthsZ(DOF); // lengths on Z axis
	MDHlengthsZ << 0.31, 0.0, 0.4, 0.0, 0.39, 0.0, 0.0;

	KinematicRobotModel = new KinematicModelMDH( DOF, MDHangles, MDHlengthsX, MDHlengthsZ, angles_EE, r_EE );
	
	//~ Inertial parameters (source: A. Jubien, M. Gautier, A. Janot. Dynamic identification of the KUKA lightweight robot: Comparison between actual and confidential KUKA’s parameters. In: 2014 IEEE/ASME International Conference on Advanced Intelligent Mechatronics. Proceedings, July, 2014, pp. 483–488)
	Eigen::MatrixXd AllRotAxes = Eigen::MatrixXd::Zero(3, DOF);
	AllRotAxes.row(2) = Eigen::RowVectorXd::Ones(DOF);
	
	Eigen::VectorXd Masses = Eigen::VectorXd::Zero(DOF);
	
	Eigen::MatrixXd AllFirstMomentsOfMass(3, DOF);
	AllFirstMomentsOfMass.col(0) = Eigen::Vector3d::Zero();
	AllFirstMomentsOfMass.col(1) = Eigen::Vector3d( 1.35 * pow(10.0, -3.0), 3.46, 0.0 );
	AllFirstMomentsOfMass.col(2) = Eigen::Vector3d( 9.45 * pow(10.0, -4.0), -4.72 * pow(10.0, -4.0), 0.0 );
	AllFirstMomentsOfMass.col(3) = Eigen::Vector3d( -3.5 * pow(10.0, -3.0), -1.33, 0.0 );
	AllFirstMomentsOfMass.col(4) = Eigen::Vector3d( -6.56 * pow(10.0, -4.0), 4.07 * pow(10.0, -2.0), 0.0 );
	AllFirstMomentsOfMass.col(5) = Eigen::Vector3d( 8.35 * pow(10.0, -4.0), 2.86 * pow(10.0, -2.0), 0.0 );
	AllFirstMomentsOfMass.col(6) = Eigen::Vector3d( -2.98 * pow(10.0, -4.0), 9.54 * pow(10.0, -4.0), 0.0 );
	
	Eigen::MatrixXd AllInertias(3, 3 * DOF);
	AllInertias.block<3,3>(0,0) << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.15 * pow(10.0, -2.0);
	AllInertias.block<3,3>(0,3) << 1.25, 0.0, 0.0, 0.0, 0.0, -5.43 * pow(10.0, -4.0), 0.0, 5.43 * pow(10.0, -4.0), 1.25;
	AllInertias.block<3,3>(0,6) << 6.36 * pow(10.0, -3.0), 0.0, 0.0, 0.0, 0.0, 7.26 * pow(10.0, -4.0), 0.0, -7.26 * pow(10.0, -4.0), 1.08 * pow(10.0, -2.0);
	AllInertias.block<3,3>(0,9) << 0.413, 0.0, 0.0, 0.0, 0.0, 5.32 * pow(10.0, -4.0), 0.0, -5.32 * pow(10.0, -4.0), 0.418;
	AllInertias.block<3,3>(0,12) << 3.95 * pow(10.0, -3.0), 0.0, 0.0, 0.0, 0.0, 4.22 * pow(10.0, -4.0), 0.0, -4.22 * pow(10.0, -4.0), 6.33 * pow(10.0, -3.0);
	AllInertias.block<3,3>(0,15) << 1.17 * pow(10.0, -3.0), 0.0, 0.0, 0.0, 0.0, 1.02 * pow(10.0, -5.0), 0.0, -1.02 * pow(10.0, -5.0), 3.77 * pow(10.0, -3.0);
	AllInertias.block<3,3>(0,18) << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.2 * pow(10.0, -4.0);
	
	Eigen::Vector3d Gravity(0.0, 0.0, -9.80665);
	
	DynamicRobotModel = new DynamicModelMDH( DOF, AllRotAxes, Masses, AllFirstMomentsOfMass, AllInertias, Gravity, MDHangles, MDHlengthsX, MDHlengthsZ );
}

RobotModel::RobotModel( int DOF, const Eigen::VectorXd &MDHangles, const Eigen::VectorXd &MDHlengthsX, const Eigen::VectorXd &MDHlengthsZ, const Eigen::Vector3d &angles_EE, const Eigen::Vector3d &r_EE, const Eigen::MatrixXd &AllRotAxes, const Eigen::VectorXd &Masses, const Eigen::MatrixXd &AllFirstMomentsOfMass, const Eigen::MatrixXd &AllInertias, const Eigen::Vector3d &Gravity ) : DOF(DOF)
{
	KinematicRobotModel = new KinematicModelMDH( DOF, MDHangles, MDHlengthsX, MDHlengthsZ, angles_EE, r_EE );
	
	DynamicRobotModel = new DynamicModelMDH( DOF, AllRotAxes, Masses, AllFirstMomentsOfMass, AllInertias, Gravity, MDHangles, MDHlengthsX, MDHlengthsZ );
}

RobotModel::RobotModel( const RobotModel &other ) : DOF(other.DOF), AccelerationPeriod(other.AccelerationPeriod), DecelerationPeriod(other.DecelerationPeriod)
{
	KinematicRobotModel = new KinematicModelMDH( *other.KinematicRobotModel );
	
	DynamicRobotModel = new DynamicModelMDH( *other.DynamicRobotModel );
}

RobotModel& RobotModel::operator= ( const RobotModel &other )
{
	if (&other == this)
	{
		return *this;
	}
	
	DOF = other.DOF;
	
	KinematicRobotModel = new KinematicModelMDH( *other.KinematicRobotModel );
	
	DynamicRobotModel = new DynamicModelMDH( *other.DynamicRobotModel );
	
	AccelerationPeriod = other.AccelerationPeriod;
	DecelerationPeriod = other.DecelerationPeriod;
	
	return *this;
}

RobotModel::~RobotModel()
{
	delete KinematicRobotModel;
	
	delete DynamicRobotModel;
}

Eigen::MatrixXd RobotModel::GetJacobian( const Eigen::VectorXd &JointPos )
{
	return KinematicRobotModel->GetJacobian( JointPos );
}

Eigen::MatrixXd RobotModel::ForwardKinematics( const Eigen::VectorXd &JointPos )
{
	return KinematicRobotModel->ForwardKinematics( JointPos );
}

Eigen::MatrixXd RobotModel::SolveInverseKinematics( const Eigen::VectorXd &InitialJointPos, const Eigen::VectorXd &InitialJointVel, const Eigen::MatrixXd& Points, const Eigen::RowVectorXd& SegmentTimes, double StartTime, double EndTime, double StepSize )
{
	int NumberOfSteps = static_cast<int>( (EndTime - StartTime) / StepSize ) + 1;
	
	Eigen::VectorXd JointPos = InitialJointPos;
	Eigen::VectorXd JointVel = InitialJointVel;
	Eigen::VectorXd JointAcc = Eigen::VectorXd::Zero(DOF);
	double CurrentTime = StartTime;
	
	//~ Declare a matrix for the results
	Eigen::MatrixXd TotalResults = Eigen::MatrixXd::Zero(NumberOfSteps, 8 + 3*DOF);
	
	//~ Initialize the trajectory generator
	Trajectory RobotTrajectory(Points, SegmentTimes, AccelerationPeriod, DecelerationPeriod, BlendingPeriod);
	
	double SubtaskConstraintGainMax = KinematicRobotModel->GetSubtaskConstraintGain();
	double SubtaskConstraintGain = 0.0;
	
	int step = 0;
	while ( step < NumberOfSteps )
	{
		//~ Start measuring time
		std::chrono::high_resolution_clock::time_point Tic = std::chrono::high_resolution_clock::now();
		
		//~ SubtaskConstraint is multiplied by a step-varying trapezoidal gain: 0.0 at the beginning, SubtaskConstraintGainMax in (StepsUntilSubtask - 1, NumberOfSteps - 1 - StepsUntilSubtask), and 0.0 at the NumberOfSteps - 1
		if (step < StepsUntilSubtask)
		{
			SubtaskConstraintGain = SubtaskConstraintGainMax * static_cast<double>(step) / StepsUntilSubtask;
		}
		else if (step > (NumberOfSteps - 1 - StepsUntilSubtask))
		{
			SubtaskConstraintGain = SubtaskConstraintGainMax * static_cast<double>(NumberOfSteps - 1 - step) / StepsUntilSubtask;
		}
		else
		{
			SubtaskConstraintGain = SubtaskConstraintGainMax;
		}
		KinematicRobotModel->SetSubtaskConstraintGain(SubtaskConstraintGain);
		
		//~ Get the desired end effector position and velocity in this step
		Eigen::VectorXd EndEffectorPosAndVel = RobotTrajectory.GetTrajectory(CurrentTime);
		Eigen::VectorXd EndEffectorPos = EndEffectorPosAndVel.head(3);
		Eigen::VectorXd EndEffectorVel = EndEffectorPosAndVel.tail(3);
		
		//~ Solve the inverse kinematics in the current step
		Eigen::MatrixXd StepState;
		if (KinematicRobotModel->GetIntegrationMethod() == EE)
		{
			StepState = KinematicRobotModel->InverseKinematicsStep(JointPos, JointVel, EndEffectorPos, EndEffectorVel, StepSize);
		}
		else
		{
			//~ Get the desired end effector velocity in the next midstep and next step (only if using the multistep ODE solver)
			Eigen::VectorXd MidEndEffectorVel = RobotTrajectory.GetTrajectory(CurrentTime + 0.5 * StepSize).tail(3);
			Eigen::VectorXd NextEndEffectorVel = RobotTrajectory.GetTrajectory(CurrentTime + StepSize).tail(3);
			
			StepState = KinematicRobotModel->InverseKinematicsStep(JointPos, JointVel, EndEffectorPos, EndEffectorVel, MidEndEffectorVel, NextEndEffectorVel, StepSize);
		}
		
		//~ Get the current results (in the next step they will be the previous step results)
		JointPos = StepState.col(0);
		JointVel = StepState.col(1);
		JointAcc = StepState.col(2);
		
		//~ Save the time, and current step joint positions, velocities and accelerations
		TotalResults(step, 0) = CurrentTime;
		TotalResults.block(step, 1, 1, DOF) = JointPos.transpose();
		TotalResults.block(step, 1 + DOF, 1, DOF) = JointVel.transpose();
		TotalResults.block(step, 1 + 2 * DOF, 1, DOF) = JointAcc.transpose();
		
		//~ Save the current step desired end effector position and velocity
		TotalResults.block(step, 1 + 3 * DOF, 1, 3) = EndEffectorPos.transpose();
		TotalResults.block(step, 4 + 3 * DOF, 1, 3) = EndEffectorVel.transpose();
		
		CurrentTime += StepSize;
		++step;
		
		//~ Stop measuring time
		std::chrono::high_resolution_clock::time_point Toc = std::chrono::high_resolution_clock::now();
		//~ Get the time elapsed during one step of the while loop
		auto WallClockTime = std::chrono::duration_cast<std::chrono::microseconds>( Toc - Tic ).count();
		TotalResults(step-1, 7 + 3 * DOF) = WallClockTime;
	}
	
	//~ Set SubtaskConstraintGain back to its user-defined value
	KinematicRobotModel->SetSubtaskConstraintGain(SubtaskConstraintGainMax);
	
	return TotalResults;
}

Eigen::MatrixXd RobotModel::SolveInverseKinematics( const Eigen::VectorXd &InitialJointPos, const Eigen::VectorXd &InitialJointVel, const Eigen::MatrixXd& Points, const Eigen::RowVectorXd& SegmentTimes, double StartTime, double EndTime, double StepSize, const Eigen::MatrixXd &ObstacleLocations )
{
	//~ Overloaded version of the SolveInverseKinematics accepting the locations of the obstacles (ObstacleLocations) as a 3 x (number of obstacles).

	int NumberOfSteps = static_cast<int>( (EndTime - StartTime) / StepSize ) + 1;
	
	Eigen::VectorXd JointPos = InitialJointPos;
	Eigen::VectorXd JointVel = InitialJointVel;
	Eigen::VectorXd JointAcc = Eigen::VectorXd::Zero(DOF);
	double CurrentTime = StartTime;
	
	//~ Declare a matrix for the results
	Eigen::MatrixXd TotalResults = Eigen::MatrixXd::Zero(NumberOfSteps, 9 + 3*DOF);
	
	//~ Initialize the trajectory generator
	Trajectory RobotTrajectory(Points, SegmentTimes, AccelerationPeriod, DecelerationPeriod, BlendingPeriod);
	
	double SubtaskConstraintGainMax = KinematicRobotModel->GetSubtaskConstraintGain();
	double SubtaskConstraintGain = 0.0;
	
	int step = 0;
	while ( step < NumberOfSteps )
	{
		//~ Start measuring time
		std::chrono::high_resolution_clock::time_point Tic = std::chrono::high_resolution_clock::now();
		
		//~ SubtaskConstraint is multiplied by a step-varying trapezoidal gain: 0.0 at the beginning, SubtaskConstraintGainMax in (StepsUntilSubtask - 1, NumberOfSteps - 1 - StepsUntilSubtask), and 0.0 at the NumberOfSteps - 1
		if (step < StepsUntilSubtask)
		{
			SubtaskConstraintGain = SubtaskConstraintGainMax * static_cast<double>(step) / StepsUntilSubtask;
		}
		else if (step > (NumberOfSteps - 1 - StepsUntilSubtask))
		{
			SubtaskConstraintGain = SubtaskConstraintGainMax * static_cast<double>(NumberOfSteps - 1 - step) / StepsUntilSubtask;
		}
		else
		{
			SubtaskConstraintGain = SubtaskConstraintGainMax;
		}
		KinematicRobotModel->SetSubtaskConstraintGain(SubtaskConstraintGain);
		
		//~ Get the obstacles' locations
		KinematicRobotModel->SetObstaclePoints(ObstacleLocations);
		//~ Save the distance to the obstacle in the current step
		Eigen::Vector3d CriticalPoint = KinematicRobotModel->ObstacleKinematics(JointPos);
		double ObstacleDistance = (CriticalPoint - ObstacleLocations.col(0)).norm();
		TotalResults(step, 7 + 3 * DOF) = ObstacleDistance;
		
		//~ Get the desired end effector position and velocity in this step
		Eigen::VectorXd EndEffectorPosAndVel = RobotTrajectory.GetTrajectory(CurrentTime);
		Eigen::VectorXd EndEffectorPos = EndEffectorPosAndVel.head(3);
		Eigen::VectorXd EndEffectorVel = EndEffectorPosAndVel.tail(3);
		
		//~ Solve the inverse kinematics in the current step
		Eigen::MatrixXd StepState;
		if (KinematicRobotModel->GetIntegrationMethod() == EE)
		{
			StepState = KinematicRobotModel->InverseKinematicsStep(JointPos, JointVel, EndEffectorPos, EndEffectorVel, StepSize);
		}
		else
		{
			//~ Get the desired end effector velocity in the next midstep and next step (only if using the multistep ODE solver)
			Eigen::VectorXd MidEndEffectorVel = RobotTrajectory.GetTrajectory(CurrentTime + 0.5 * StepSize).tail(3);
			Eigen::VectorXd NextEndEffectorVel = RobotTrajectory.GetTrajectory(CurrentTime + StepSize).tail(3);
			
			StepState = KinematicRobotModel->InverseKinematicsStep(JointPos, JointVel, EndEffectorPos, EndEffectorVel, MidEndEffectorVel, NextEndEffectorVel, StepSize);
		}
		
		//~ Get the current results (in the next step they will be the previous step results)
		JointPos = StepState.col(0);
		JointVel = StepState.col(1);
		JointAcc = StepState.col(2);
		
		//~ Save the time, and current step joint positions, velocities and accelerations
		TotalResults(step, 0) = CurrentTime;
		TotalResults.block(step, 1, 1, DOF) = JointPos.transpose();
		TotalResults.block(step, 1 + DOF, 1, DOF) = JointVel.transpose();
		TotalResults.block(step, 1 + 2 * DOF, 1, DOF) = JointAcc.transpose();
		
		//~ Save the current step desired end effector position and velocity
		TotalResults.block(step, 1 + 3 * DOF, 1, 3) = EndEffectorPos.transpose();
		TotalResults.block(step, 4 + 3 * DOF, 1, 3) = EndEffectorVel.transpose();
		
		CurrentTime += StepSize;
		++step;
		
		//~ Stop measuring time
		std::chrono::high_resolution_clock::time_point Toc = std::chrono::high_resolution_clock::now();
		//~ Get the time elapsed during one step of the while loop
		auto WallClockTime = std::chrono::duration_cast<std::chrono::microseconds>( Toc - Tic ).count();
		TotalResults(step-1, 8 + 3 * DOF) = WallClockTime;
	}
	
	//~ Set SubtaskConstraintGain back to its user-defined value
	KinematicRobotModel->SetSubtaskConstraintGain(SubtaskConstraintGainMax);
	
	return TotalResults;
}

void RobotModel::SetNewTool( const Eigen::Vector3d &New_angles_EE, const Eigen::Vector3d &New_r_EE )
{
	KinematicRobotModel->SetNewTool( New_angles_EE, New_r_EE );
}

Eigen::MatrixXd RobotModel::SolveInverseDynamics( const Eigen::MatrixXd &JointPosVelAccTotal )
{
	int NumberOfSteps = JointPosVelAccTotal.rows();
	
	Eigen::MatrixXd TotalResults = Eigen::MatrixXd::Zero(NumberOfSteps, DOF);
	
	int step = 0;
	while ( step < NumberOfSteps )
	{
		Eigen::VectorXd JointPos = JointPosVelAccTotal.block(step, 0, 1, DOF).transpose();
		Eigen::VectorXd JointVel = JointPosVelAccTotal.block(step, DOF, 1, DOF).transpose();
		Eigen::VectorXd JointAcc = JointPosVelAccTotal.block(step, 2 * DOF, 1, DOF).transpose();
		
		Eigen::VectorXd JointTrq = DynamicRobotModel->InverseDynamicsStep(JointPos, JointVel, JointAcc);
		TotalResults.row(step) = JointTrq.transpose();
		
		++step;
	}
	
	return TotalResults;
}

void RobotModel::SetPseudoInverse( pinvType HowToInvertJacobian, double FirstValue, double SecondValue )
{
	//~ Allows to manage the parameters used in pseudoinversion of the Jacobian matrix
	KinematicRobotModel->SetPseudoInverse( HowToInvertJacobian, FirstValue, SecondValue );
}

void RobotModel::SetErrorGain( double ErrorGain )
{
	//~ Set the error gain for the CLIK method
	KinematicRobotModel->SetErrorGain(ErrorGain);
}

void RobotModel::SetErrorSaturation( double ErrorSaturation )
{
	//~ Set the saturation level of error for the CLIK method
	KinematicRobotModel->SetErrorSaturation(ErrorSaturation);
}

void RobotModel::SetIntegrationMethod( integrationType IntegrationMethod )
{
	//~ Set the integration method: EE -- explicit Euler, EM -- explicit midpoint, RK4 -- Runge-Kutta 4th order
	KinematicRobotModel->SetIntegrationMethod(IntegrationMethod);
}

void RobotModel::SetSubtask( subtaskType Subtask )
{
	//~ Set the secondary task in the redundancy resolution
	KinematicRobotModel->SetSubtask(Subtask);
}

void RobotModel::SetSubtaskConstraintGain( double SubtaskConstraintGain )
{
	//~ Set the value of the subtask constraint gain (it scales the nullspace velocity)
	KinematicRobotModel->SetSubtaskConstraintGain(SubtaskConstraintGain);
}

void RobotModel::SetSubtaskConstraintGainAndSteps( double SubtaskConstraintGain, int NewStepsUntilSubtask )
{
	//~ Set the value of the subtask constraint gain (it scales the nullspace velocity), and the number of steps until it reaches the full value
	KinematicRobotModel->SetSubtaskConstraintGain(SubtaskConstraintGain);
	StepsUntilSubtask = NewStepsUntilSubtask;
}

void RobotModel::SetMultitaskType( multitaskType MultitaskType )
{
	//~ Set the type of multitask formulation
	KinematicRobotModel->SetMultitaskType(MultitaskType);
}

void RobotModel::SetJointLimits( const Eigen::VectorXd &JointLimitMax, const Eigen::VectorXd &JointLimitMin )
{
	//~ Set the joint limits
	KinematicRobotModel->SetJointLimits(JointLimitMax, JointLimitMin);
}

void RobotModel::SetObstaclePoints( const Eigen::MatrixXd ObstacleLocations )
{
	//~ Set the obstacles' locations
	KinematicRobotModel->SetObstaclePoints(ObstacleLocations);
}

void RobotModel::SetObstacleAvoidanceParameters( double ObstacleSphereOfInfluence, double ObstacleUnityGain, double ObstacleAvoidanceNominalVel )
{
	//~ Set the parameters of the obstacle avoidance subtask
	KinematicRobotModel->SetObstacleAvoidanceParameters(ObstacleSphereOfInfluence, ObstacleUnityGain, ObstacleAvoidanceNominalVel);
}

void RobotModel::SetAccelerationDecelerationAndBlendingPeriod( double NewAccelerationPeriod, double NewDecelerationPeriod, double NewBlendingPeriod )
{
	//~ Declare how much of the segment time is allocated for acceleration and deceleration
	AccelerationPeriod = NewAccelerationPeriod;
	DecelerationPeriod = NewDecelerationPeriod;
	
	//~ Set the fraction of the segment's time to blend two linear segments of the trajectory
	BlendingPeriod = NewBlendingPeriod;
}

void RobotModel::SetAccelerationAndDecelerationPeriod( double NewAccelerationPeriod, double NewDecelerationPeriod )
{
	//~ Declare how much of the segment time is allocated for acceleration and deceleration
	AccelerationPeriod = NewAccelerationPeriod;
	DecelerationPeriod = NewDecelerationPeriod;
}

} //namespace RedundantRobot
