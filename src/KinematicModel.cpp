//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "RedundantRobot/KinematicModel.h"

namespace RedundantRobot
{

KinematicModel::KinematicModel( int DOF )
: DOF(DOF)
{
	KinematicSettings.JointLimitMax = Eigen::VectorXd::Zero(DOF);
	KinematicSettings.JointLimitMin = Eigen::VectorXd::Zero(DOF);
	KinematicSettings.JointMid = 0.5 * (KinematicSettings.JointLimitMax + KinematicSettings.JointLimitMin);
}

KinematicModel::KinematicModel( const KinematicModel &other )
: DOF(other.DOF), KinematicSettings(other.KinematicSettings)
{
}

KinematicModel& KinematicModel::operator= ( const KinematicModel &other )
{
	if (&other == this) // self-assignment
	{
		return *this;
	}
	
	DOF = other.DOF;
	KinematicSettings = other.KinematicSettings;
	
	return *this;
}

KinematicModel::~KinematicModel()
{
}

Eigen::MatrixXd KinematicModel::InverseKinematicsStep( const Eigen::VectorXd &PrevJointPos, const Eigen::VectorXd &PrevJointVel, const Eigen::VectorXd &EndEffectorPos, const Eigen::VectorXd &EndEffectorVel, double StepSize )
{
	//~ Computes one step of the inverse kinematics problem and returns joint positions, velocities and accelerations -- a state to be sent to the robot controller as the desired state to achieve.
	//~ Needs joint positions and velocities from the previous step.
	//~ If this is a first step and only the previous joint positions are known then just pass zeroes as previous joint velocities.
	
	//~ Joint positions
	Eigen::VectorXd JointPos = PrevJointPos + PrevJointVel * StepSize;
	
	//~ Vector for the joint velocities
	Eigen::VectorXd JointVel = Eigen::VectorXd::Zero(DOF);
	
	Eigen::VectorXd EndEffectorError = GetEndEffectorError(JointPos, EndEffectorPos);
	if (EndEffectorError.norm() >= KinematicSettings.ErrorSaturation)
	{
		EndEffectorError = EndEffectorError / EndEffectorError.norm() * KinematicSettings.ErrorSaturation;
	}
	
	//~ Joint velocities (explicit Euler method)
	JointVel = GetJointVel( JointPos, EndEffectorVel + KinematicSettings.ErrorGain * EndEffectorError );
	
	//~ Temporary hack
	//~ Eigen::VectorXd JointVelMax = 2.5 * Eigen::VectorXd::Ones(DOF);
	//~ Eigen::VectorXd JointVelMin = -JointVelMax;
	//~ for (int j = 0; j < DOF; ++j)
	//~ {
		//~ if (JointVel(j) > JointVelMax(j))
		//~ {
			//~ JointVel(j) = JointVelMax(j);
		//~ }
		//~ else if (JointVel(j) < JointVelMin(j))
		//~ {
			//~ JointVel(j) = JointVelMin(j);
		//~ }
	//~ }

	//~ Joint accelerations
	Eigen::VectorXd JointAcc = ( JointVel - PrevJointVel ) / StepSize;

	//~ Desired state -- to be sent to the robot controller
	Eigen::MatrixXd MotionState(DOF, 3);
	MotionState.col(0) = JointPos;
	MotionState.col(1) = JointVel;
	MotionState.col(2) = JointAcc;
	return MotionState;
}

Eigen::MatrixXd KinematicModel::InverseKinematicsStep( const Eigen::VectorXd &PrevJointPos, const Eigen::VectorXd &PrevJointVel, const Eigen::VectorXd &EndEffectorPos, const Eigen::VectorXd &EndEffectorVel, const Eigen::VectorXd &MidEndEffectorVel, const Eigen::VectorXd &NextEndEffectorVel, double StepSize )
{
	//~ Overloaded version for multistep ODE solvers
	//~ Computes one step of the inverse kinematics problem and returns joint positions, velocities and accelerations -- a state to be sent to the robot controller as the desired state to achieve.
	//~ Needs joint positions and velocities from the previous step.
	//~ If this is a first step and only the previous joint positions are known then just pass zeroes as previous joint velocities.
	
	//~ Joint positions
	Eigen::VectorXd JointPos = PrevJointPos + PrevJointVel * StepSize;
	
	//~ Vector for the joint velocities
	Eigen::VectorXd JointVel = Eigen::VectorXd::Zero(DOF);
	
	Eigen::VectorXd EndEffectorError = GetEndEffectorError(JointPos, EndEffectorPos);
	if (EndEffectorError.norm() >= KinematicSettings.ErrorSaturation)
	{
		EndEffectorError = EndEffectorError / EndEffectorError.norm() * KinematicSettings.ErrorSaturation;
	}
	
	switch(KinematicSettings.IntegrationMethod)
	{
		case EE:
			{
				//~ Explicit Euler method
				JointVel = GetJointVel( JointPos, EndEffectorVel + KinematicSettings.ErrorGain * EndEffectorError );
			}
			break;
		case EM:
			{
				//~ Explicit midpoint method
				JointVel = ExplicitMidpoint( JointPos, EndEffectorError, EndEffectorVel, MidEndEffectorVel, StepSize );
			}
			break;
		case ET:
			{
				//~ Heun's method (explicit trapezoidal method)
				JointVel = ExplicitTrapezoidal( JointPos, EndEffectorError, EndEffectorVel, NextEndEffectorVel, StepSize );
			}
			break;
		case RK4:
			{
				//~ Explicit Runge-Kutta 4th order method
				JointVel = RungeKutta4( JointPos, EndEffectorError, EndEffectorVel, MidEndEffectorVel, NextEndEffectorVel, StepSize );
			}
			break;
	}
	
	//~ Temporary hack
	//~ Eigen::VectorXd JointVelMax = 2.5 * Eigen::VectorXd::Ones(DOF);
	//~ Eigen::VectorXd JointVelMin = -JointVelMax;
	//~ for (int j = 0; j < DOF; ++j)
	//~ {
		//~ if (JointVel(j) > JointVelMax(j))
		//~ {
			//~ JointVel(j) = JointVelMax(j);
		//~ }
		//~ else if (JointVel(j) < JointVelMin(j))
		//~ {
			//~ JointVel(j) = JointVelMin(j);
		//~ }
	//~ }

	//~ Joint accelerations
	Eigen::VectorXd JointAcc = ( JointVel - PrevJointVel ) / StepSize;

	//~ Desired state -- to be sent to the robot controller
	Eigen::MatrixXd MotionState(DOF, 3);
	MotionState.col(0) = JointPos;
	MotionState.col(1) = JointVel;
	MotionState.col(2) = JointAcc;
	return MotionState;
}

Eigen::VectorXd KinematicModel::GetJointVel( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &EndEffectorVel )
{
	//~ Compute the joint velocities vector
	
	Eigen::VectorXd JointVel(DOF);
	
	//~ Compute the Jacobian
	Eigen::MatrixXd Jacobian = GetJacobian( JointPos );
	//~ Compute the Jacobian pseudoinverse and projector matrix
	Eigen::MatrixXd InvertedJacobianAndProjector = JacobianPseudoInverseAndProjector(Jacobian);
	Eigen::MatrixXd InvertedJacobian = InvertedJacobianAndProjector.block(0, 0, Jacobian.cols(), Jacobian.rows());
	Eigen::MatrixXd Projector = InvertedJacobianAndProjector.block(0, Jacobian.rows(), DOF, DOF);
	Eigen::VectorXd MainTaskJointVel = InvertedJacobian * EndEffectorVel;
	//~ Compute the joint velocities
	JointVel = MainTaskJointVel + GetNullSpaceVel( JointPos, MainTaskJointVel, Projector );
	
	return JointVel;
}

Eigen::VectorXd KinematicModel::ExplicitMidpoint( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &EndEffectorError, const Eigen::VectorXd &EndEffectorVel, const Eigen::VectorXd &MidEndEffectorVel, double StepSize )
{
	//~ Explicit midpoint method
	
	Eigen::VectorXd JointVel1 = GetJointVel( JointPos, EndEffectorVel + KinematicSettings.ErrorGain * EndEffectorError );
	Eigen::VectorXd JointVel2 = GetJointVel( JointPos + 0.5 * JointVel1 * StepSize, MidEndEffectorVel );
	
	return JointVel2;
}

Eigen::VectorXd KinematicModel::ExplicitTrapezoidal( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &EndEffectorError, const Eigen::VectorXd &EndEffectorVel, const Eigen::VectorXd &NextEndEffectorVel, double StepSize )
{
	//~ Heun's method (explicit trapezoidal method)
	
	Eigen::VectorXd JointVel1 = GetJointVel( JointPos, EndEffectorVel + KinematicSettings.ErrorGain * EndEffectorError );
	Eigen::VectorXd JointVel2 = GetJointVel( JointPos + JointVel1 * StepSize, NextEndEffectorVel );
	
	return (JointVel1 + JointVel2) / 2.0;
}

Eigen::VectorXd KinematicModel::RungeKutta4( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &EndEffectorError, const Eigen::VectorXd &EndEffectorVel, const Eigen::VectorXd &MidEndEffectorVel, const Eigen::VectorXd &NextEndEffectorVel, double StepSize )
{
	//~ Explicit Runge-Kutta 4th order method
	
	Eigen::VectorXd JointVel1 = GetJointVel( JointPos, EndEffectorVel + KinematicSettings.ErrorGain * EndEffectorError );
	Eigen::VectorXd JointVel2 = GetJointVel( JointPos + 0.5 * JointVel1 * StepSize, MidEndEffectorVel );
	Eigen::VectorXd JointVel3 = GetJointVel( JointPos + 0.5 * JointVel2 * StepSize, MidEndEffectorVel );
	Eigen::VectorXd JointVel4 = GetJointVel( JointPos + JointVel3 * StepSize, NextEndEffectorVel );
	
	return (JointVel1 + 2.0 * JointVel2 + 2.0 * JointVel3 + JointVel4) / 6.0;
}

Eigen::MatrixXd KinematicModel::JacobianPseudoInverseAndProjector( const Eigen::MatrixXd &Jacobian )
{
	//~ Computes the pseudoinverse of the jacobian matrix (InvertedJacobian) and the projector matrix for the subtask (Projector).
	//~ Instead of returning InvertedJacobian and Projector, the functions below modify them as they are passed by (non-const) reference.
	
	//~ Initialize the Jacobian pseudoinverse and the projector matrix with zeroes
	Eigen::MatrixXd InvertedJacobian = Eigen::MatrixXd::Zero(Jacobian.cols(), Jacobian.rows());
	Eigen::MatrixXd Projector = Eigen::MatrixXd::Zero(DOF, DOF);
	
	switch(KinematicSettings.HowToInvertJacobian)
	{
		case JP:
			{
				//~ JP: Jacobian Pseudoinverse; inverts the Jacobian matrix using QR
				InvertedJacobian = Jacobian.completeOrthogonalDecomposition().pseudoInverse();
				Projector = Eigen::MatrixXd::Identity(DOF, DOF) - InvertedJacobian * Jacobian;
			}
			break;
		case JD:
			{
				JacobianDamping(Jacobian, InvertedJacobian, Projector);
			}
			break;
		case SVF:
			{
				SingularValueFiltering(Jacobian, InvertedJacobian, Projector);
			}
			break;
		case SVFmod:
			{
				SingularValueFilteringModified(Jacobian, InvertedJacobian, Projector);
			}
			break;
		case JT:
			{
				//~ JT: Jacobian Transpose; transposes the Jacobian matrix instead of inverting
				InvertedJacobian = Jacobian.transpose();
				Projector = Eigen::MatrixXd::Identity(DOF, DOF) - InvertedJacobian * Jacobian;
			}
			break;
	}
	
	Eigen::MatrixXd InvertedJacobianAndProjector(Jacobian.cols() + DOF, Jacobian.rows() + DOF);
	InvertedJacobianAndProjector.block(0, 0, Jacobian.cols(), Jacobian.rows()) = InvertedJacobian;
	InvertedJacobianAndProjector.block(0, Jacobian.rows(), DOF, DOF) = Projector;
	
	return InvertedJacobianAndProjector;
}

void KinematicModel::JacobianDamping( const Eigen::MatrixXd &Jacobian, Eigen::MatrixXd &InvertedJacobian, Eigen::MatrixXd &Projector )
{
	//~ JD: Jacobian Damping; inverts the Jacobian matrix using SVD and least-squares damping to avoid problems near and at singular configurations (where the smallest singular value tends to zero)
	//~ Instead of returning InvertedJacobian and Projector, the function modifies them as they are passed by (non-const) reference.

	Eigen::JacobiSVD<Eigen::MatrixXd> svd(Jacobian, Eigen::ComputeFullU | Eigen::ComputeFullV );	
	Eigen::MatrixXd pinvS = Eigen::MatrixXd::Zero(DOF, svd.singularValues().size());
	
	double DampingFactorMaxSquared = KinematicSettings.DampingFactorMaxSquared;
	double SingularValueTreshold = KinematicSettings.SingularValueTreshold;
	
	//~ Calculate the variable damping factor
	double SmallestSingularValue = svd.singularValues().tail(1)[0]; // The last of the singular values is the smallest -- so we need the block containing 1 last element. Then we need to extract the first (and only) element of that block, beacause we need a 'double' variable
	double DampingFactorSquared = 0.0;
	if ( SmallestSingularValue < SingularValueTreshold )
	{
		DampingFactorSquared = (1.0 - (SmallestSingularValue / SingularValueTreshold) * (SmallestSingularValue / SingularValueTreshold) ) * DampingFactorMaxSquared;
	}
	
	//~ Calculate the inverse of the diagonal matrix of the Jacobian's singular values
	for (int i = 0; i < svd.singularValues().size(); ++i)
	{
		pinvS(i, i) = svd.singularValues()[i] / ( svd.singularValues()[i] * svd.singularValues()[i] + DampingFactorSquared );
	}
	
	InvertedJacobian = svd.matrixV() * pinvS * svd.matrixU().transpose();
	Projector = Eigen::MatrixXd::Identity(DOF, DOF) - InvertedJacobian * Jacobian; // TODO: use the explicit equation based on SVD
}

void KinematicModel::SingularValueFiltering( const Eigen::MatrixXd &Jacobian, Eigen::MatrixXd &InvertedJacobian, Eigen::MatrixXd &Projector )
{
	//~ SVF: Singular Value Filtering; inverts the Jacobian matrix using SVD and singular value filtering to avoid problems near and at singular configurations (where the smallest singular value tends to zero); A. Colomé and C. Torras. “Closed-Loop Inverse Kinematics for Redundant Robots: Comparative Assessment and Two Enhancements”. In: IEEE/ASME Transactions on Mechatronics 20.2 (Apr. 2015), pp. 944–955. issn: 1083-4435. doi: 10.1109/TMECH.2014.2326304 .
	
	//~ Instead of returning InvertedJacobian and Projector, the function modifies them as they are passed by (non-const) reference.
			
	Eigen::JacobiSVD<Eigen::MatrixXd> svd(Jacobian, Eigen::ComputeFullU | Eigen::ComputeFullV );	
	Eigen::MatrixXd pinvS = Eigen::MatrixXd::Zero(DOF, svd.singularValues().size());
	
	double SingularValueFiltered = 0.0;
	double AssignedSmallestSingularValue = KinematicSettings.AssignedSmallestSingularValue;
	double SVFshapeFactor = KinematicSettings.SVFshapeFactor;
	
	for (int i = 0; i < Jacobian.rows(); ++i)
	{
		SingularValueFiltered = ( svd.singularValues()[i] * svd.singularValues()[i] * svd.singularValues()[i] + SVFshapeFactor * svd.singularValues()[i] * svd.singularValues()[i] + 2.0 * svd.singularValues()[i] + 2.0 * AssignedSmallestSingularValue ) / ( svd.singularValues()[i] * svd.singularValues()[i] + SVFshapeFactor * svd.singularValues()[i] + 2.0 );
		
		pinvS(i, i) = 1.0 / SingularValueFiltered;
	}
	
	InvertedJacobian = svd.matrixV() * pinvS * svd.matrixU().transpose();
	Projector = Eigen::MatrixXd::Identity(DOF, DOF) - InvertedJacobian * Jacobian; // TODO: use the explicit equation based on SVD
}

void KinematicModel::SingularValueFilteringModified( const Eigen::MatrixXd &Jacobian, Eigen::MatrixXd &InvertedJacobian, Eigen::MatrixXd &Projector )
{
	//~ SVFmod: modified Singular Value Filtering; inverts the Jacobian matrix using SVD and modified singular value filtering to avoid problems near and at singular configurations (where the smallest singular value tends to zero); P. Mogilnicki. Generowanie trajektorii dla robota redundantnego KUKA LWR4+ (Trajectory generation for KUKA LWR4+ redundant robot). (in Polish). Bachelor’s thesis. Warsaw University of Technology. Jan. 2016.
	
	//~ Instead of returning InvertedJacobian and Projector, the function modifies them as they are passed by (non-const) reference.

	Eigen::JacobiSVD<Eigen::MatrixXd> svd(Jacobian, Eigen::ComputeFullU | Eigen::ComputeFullV );	
	Eigen::MatrixXd pinvS = Eigen::MatrixXd::Zero(DOF, svd.singularValues().size());
	
	double SingularValueFiltered = 0.0;
	double AssignedSmallestSingularValue = KinematicSettings.AssignedSmallestSingularValue;
	double SingularValueTreshold = KinematicSettings.SingularValueTreshold;
	
	for (int i = 0; i < Jacobian.rows(); ++i)
	{
		if ( svd.singularValues()[i] < SingularValueTreshold )
		{	
			SingularValueFiltered = ( 2 * AssignedSmallestSingularValue - SingularValueTreshold ) / ( SingularValueTreshold * SingularValueTreshold * SingularValueTreshold ) * svd.singularValues()[i] * svd.singularValues()[i] * svd.singularValues()[i] +  ( 2 * SingularValueTreshold - 3 * AssignedSmallestSingularValue ) / ( SingularValueTreshold * SingularValueTreshold ) * svd.singularValues()[i] * svd.singularValues()[i] + AssignedSmallestSingularValue;
		}
		else
		{
			SingularValueFiltered = svd.singularValues()[i];
		}
		
		pinvS(i, i) = 1.0 / SingularValueFiltered;
	}
	
	InvertedJacobian = svd.matrixV() * pinvS * svd.matrixU().transpose();
	Projector = Eigen::MatrixXd::Identity(DOF, DOF) - InvertedJacobian * Jacobian; // TODO: use the explicit equation based on SVD
}

Eigen::VectorXd KinematicModel::GetNullSpaceVel( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &MainTaskJointVel, const Eigen::MatrixXd &Projector )
{
	//~ Computes the null space velocity vector	associated with the subtask (secondary priority to the main task) to make use of the redundancy of the kinematic chain
	
	Eigen::VectorXd NullSpaceVel(DOF);
	
	switch(KinematicSettings.Subtask)
	{
		case JointLimitGradient:
			{
				NullSpaceVel = Projector * KinematicSettings.SubtaskConstraintGain * GetJointLimitGradient( JointPos );
			}
			break;
		case JointLimit:
			{	
				NullSpaceVel = Projector * KinematicSettings.SubtaskConstraintGain * GetJointLimit( JointPos );
			}
			break;
		case ObstacleAvoidance:
			{
				//~ NullSpaceVel = GetObstacleAvoidance( JointPos, MainTaskJointVel, Jacobian, Projector ).col(0);
				NullSpaceVel = GetObstacleAvoidance( JointPos, MainTaskJointVel, Projector ).col(0);
			}
			break;
		case MultipleSubtasks:
			{
				//~ Get the null space velocity and projector of the first subtask -- obstacle avoidance
				//~ Eigen::MatrixXd NullSpaceVelAndSubtaskProjector = GetObstacleAvoidance( JointPos, MainTaskJointVel, Jacobian, Projector );
				Eigen::MatrixXd NullSpaceVelAndSubtaskProjector = GetObstacleAvoidance( JointPos, MainTaskJointVel, Projector );
				Eigen::VectorXd ObstacleAvoidanceNullSpaceVel = NullSpaceVelAndSubtaskProjector.col(0);
				Eigen::MatrixXd ObstacleAvoidanceNullSpaceProjector = NullSpaceVelAndSubtaskProjector.rightCols(DOF);
				
				//~ Get the velocity of the second subtask -- avoiding joint limits -- and project in onto the null space of the previous tasks (main and obstacle avoidance). Add it to the first subtask null space velocity and return the sum.
				NullSpaceVel = ObstacleAvoidanceNullSpaceVel + ObstacleAvoidanceNullSpaceProjector * KinematicSettings.SubtaskConstraintGain * GetJointLimit( JointPos );
			}
			break;
		case None:
			{
				NullSpaceVel = Eigen::VectorXd::Zero(DOF);
			}
			break;
		default:
			{
				NullSpaceVel = Eigen::VectorXd::Zero(DOF);
			}
	}
	
	return NullSpaceVel;
}

Eigen::VectorXd KinematicModel::GetJointLimitGradient( const Eigen::VectorXd &JointPos )
{
	Eigen::VectorXd JointMax = KinematicSettings.JointLimitMax;
	Eigen::VectorXd JointMin = KinematicSettings.JointLimitMin;
	Eigen::VectorXd JointMid = KinematicSettings.JointMid;
	
	Eigen::VectorXd SubtaskVel(DOF);
	for (int j = 0; j < DOF; ++j)
	{
		SubtaskVel(j) = -( JointPos(j) - JointMid(j) ) / ( ( JointMax(j) - JointMin(j) ) * ( JointMax(j) - JointMin(j) ) );
	}
	
	return SubtaskVel;
}

Eigen::VectorXd KinematicModel::GetJointLimit( const Eigen::VectorXd &JointPos )
{	
	//~ Abdelrahem Atawnih, Dimitrios Papageorgiou, and Zoe Doulgeri. “Kinematic control of redundant robots with guaranteed joint limit avoidance”. In: Robotics and Autonomous Systems 79 (2016), pp. 122–131. issn: 0921-8890. doi: https : / / doi . org / 10 . 1016 / j . robot . 2016 . 01 . 006 . url: http : / / www . sciencedirect . com / science / article / pii / S0921889016000130
	//~ The method is sensitive to the value of the SubtaskConstraintGain. Moreover, the starting position of the robot should be as far away from the joint limits as possible.
	
	Eigen::VectorXd JointMax = KinematicSettings.JointLimitMax;
	Eigen::VectorXd JointMin = KinematicSettings.JointLimitMin;
	Eigen::VectorXd JointMid = KinematicSettings.JointMid;

	Eigen::VectorXd SubtaskVel = Eigen::VectorXd::Zero(DOF);
	for (int j = 0; j < DOF; ++j)
	{
		double Var = ( JointPos(j) - JointMid(j) ) / ( 0.5 * ( JointMax(j) - JointMin(j) ) );
		//~ A hack to avoid the risk of log(0/2) or log(2/0)
		if (abs(Var) >= 1.0)
		{
			Var = 0.9999999;
		}
		SubtaskVel(j) = -2.0 / ( ( 1.0 - Var * Var) * 0.5 * ( JointMax(j) - JointMin(j) ) ) * log( (1.0 + Var) / (1.0 - Var) );
	}
	return SubtaskVel;
}

Eigen::MatrixXd KinematicModel::GetObstacleAvoidance( const Eigen::VectorXd &JointPos, const Eigen::VectorXd &MainTaskJointVel, const Eigen::MatrixXd &Projector )
{
	//~ T. Petrič et al. “Obstacle Avoidance with Industrial Robots”. In: Motion and Operation Planning of Robotic Systems: Background and Practical Approaches. Ed. by Giuseppe Carbone and Fernando Gomez-Bravo. Cham: Springer International Publishing, 2015, pp. 113–145. isbn: 978-3-319-14705-5. doi: 10.1007/978-3-319-14705-5_5 . url: https://doi.org/10.1007/978-3-319-14705-5_5
	
	//~ Anthony A. Maciejewski and Charles A. Klein. “Obstacle Avoidance for Kinematically Redundant Manipulators in Dynamically Varying Environments”. In: The International Journal of Robotics Research 4.3 (1985), pp. 109–117. doi: 10.1177/027836498500400308 . eprint: https://doi.org/10.1177/027836498500400308 . url: https://doi.org/10.1177/027836498500400308 .
	
	Eigen::VectorXd NullSpaceVel(DOF);
	Eigen::MatrixXd SubtaskNullSpaceProjector = Eigen::MatrixXd::Zero(DOF, DOF);
	
	//~ Get the location of the critical point on the robot
	Eigen::Vector3d CriticalPoint = ObstacleKinematics( JointPos );
	
	//~ Get the location of the obstacle
	Eigen::Vector3d ObstacleLocation = KinematicSettings.ObstaclePoints.col(0);
	
	//~ Compute the vector connecting the critical point on the robot with the point on the obstacle
	Eigen::Vector3d ObstacleVector = CriticalPoint - ObstacleLocation;
	//~ Eigen::VectorXd ObstacleVector = Eigen::VectorXd::Ones(3);
	
	//~ Compute the unit vector in the direction between the critical point and the obstacle
	double ObstacleDistance = ObstacleVector.norm();
	Eigen::Vector3d ObstacleUnitVector = ObstacleVector / ObstacleDistance;
	
	if (ObstacleDistance < KinematicSettings.ObstacleSphereOfInfluence)
	{
		//~ Compute the ObstacleSmoothingFactor
		double ObstacleSmoothingFactor;
		if (ObstacleDistance <= KinematicSettings.ObstacleUnityGain)
		{
			ObstacleSmoothingFactor = 1.0;
		}
		else
		{
			ObstacleSmoothingFactor = 0.5 * ( 1.0 - cos( M_PI * (ObstacleDistance - KinematicSettings.ObstacleSphereOfInfluence) / (KinematicSettings.ObstacleUnityGain - KinematicSettings.ObstacleSphereOfInfluence) ) );
		}
		
		//~ Compute the ObstacleAvoidanceVel
		double ObstacleAvoidanceVelGain;
		if (ObstacleDistance < KinematicSettings.ObstacleUnityGain)
		{
			ObstacleAvoidanceVelGain = (KinematicSettings.ObstacleUnityGain / ObstacleDistance) * (KinematicSettings.ObstacleUnityGain / ObstacleDistance) - 1.0;
		}
		else
		{
			ObstacleAvoidanceVelGain = 0.0;
		}
		Eigen::VectorXd ObstacleAvoidanceVel(1);
		ObstacleAvoidanceVel(0) = ObstacleAvoidanceVelGain * KinematicSettings.ObstacleAvoidanceNominalVel;
		
		//~ Compute the obstacle avoidance task Jacobian
		Eigen::MatrixXd ObstacleJacobian = ObstacleUnitVector.transpose() * GetObstacleJacobian( JointPos );
		
		if (KinematicSettings.MultitaskType == Standard)
		{
			//~ Start computing the obstacle avoidance task projector by multiplying the task Jacobian and the null space projector of the main task
			Eigen::MatrixXd ObstacleJacobianProjector = ObstacleJacobian * Projector;
			//~ Invert the ObstacleJacobianProjector
			double SquaredSum = 0.0;
			for (int j = 0; j < DOF; ++j)
			{
				SquaredSum += ObstacleJacobianProjector(j) * ObstacleJacobianProjector(j);
			}
			if (SquaredSum < 0.000001)
			{
				SquaredSum = 0.000001;
			}
			Eigen::MatrixXd ObstacleJacobianProjectorInverted = ObstacleJacobianProjector.transpose() * (1 / SquaredSum);
			
			//~ Compute the null space velocity of the obstacle avoidance task
			NullSpaceVel = ObstacleSmoothingFactor * ObstacleJacobianProjectorInverted * (ObstacleAvoidanceVel - ObstacleJacobian * MainTaskJointVel);
			
			//~ Compute the null space projector (it is necessary if there are other subtasks to project onto the null space of the higher priority tasks and the current one)
			if (KinematicSettings.Subtask == MultipleSubtasks)
			{
				SubtaskNullSpaceProjector = Projector - ObstacleSmoothingFactor * ObstacleJacobianProjectorInverted * ObstacleJacobianProjector;
			}
		}
		else if (KinematicSettings.MultitaskType == SingularityRobust)
		{
			//~ S. Chiaverini. “Singularity-robust task-priority redundancy resolution for real-time kinematic control of robot manipulators”. In: IEEE Transactions on Robotics and Automation 13.3 (June 1997), pp. 398–410. issn: 1042-296X. doi: 10.1109/70.585902 .
			
			//~ Invert the ObstacleJacobian
			double SquaredSum = 0.0;
			for (int j = 0; j < DOF; ++j)
			{
				SquaredSum += ObstacleJacobian(j) * ObstacleJacobian(j);
			}
			if (SquaredSum < 0.000001)
			{
				SquaredSum = 0.000001;
			}
			Eigen::MatrixXd InvertedObstacleJacobian = ObstacleJacobian.transpose() * (1 / SquaredSum);
			//~ Compute the null space velocity of the obstacle avoidance task
			NullSpaceVel = ObstacleSmoothingFactor * Projector * InvertedObstacleJacobian * ObstacleAvoidanceVel;
			
			//~ Compute the null space projector (it is necessary if there are other subtasks to project onto the null space of the higher priority tasks and the current one)
			if (KinematicSettings.Subtask == MultipleSubtasks)
			{
				Eigen::MatrixXd ObstacleJacobianProjector = ObstacleJacobian * Projector;
				//~ Invert the ObstacleJacobianProjector
				SquaredSum = 0.0;
				for (int j = 0; j < DOF; ++j)
				{
					SquaredSum += ObstacleJacobianProjector(j) * ObstacleJacobianProjector(j);
				}
				if (SquaredSum < 0.000001)
				{
					SquaredSum = 0.000001;
				}
				Eigen::MatrixXd ObstacleJacobianProjectorInverted = ObstacleJacobianProjector.transpose() * (1 / SquaredSum);
				
				SubtaskNullSpaceProjector = Projector - ObstacleSmoothingFactor * ObstacleJacobianProjectorInverted * ObstacleJacobianProjector;
			}
		}
	}
	else
	{
		//~ If outside the SphereOfInfluence, then the null space velocity is zero, and the null space projector is equal to the null space projector of higher priority tasks
		NullSpaceVel = Eigen::VectorXd::Zero(DOF);
		SubtaskNullSpaceProjector = Projector;
	}
	
	//~ Pack the result to one matrix
	Eigen::MatrixXd NullSpaceVelAndSubtaskProjector(DOF, 1 + DOF);
	NullSpaceVelAndSubtaskProjector.col(0) = NullSpaceVel;
	NullSpaceVelAndSubtaskProjector.rightCols(DOF) = SubtaskNullSpaceProjector;
	
	return NullSpaceVelAndSubtaskProjector;
}

void KinematicModel::SetPseudoInverse( pinvType HowToInvertJacobian, double FirstValue, double SecondValue )
{
	//~ Allows to manage the parameters used in pseudoinversion of the Jacobian matrix
	//~ FirstValue and SecondValue are the values of the parameters associated with the damped least-squares pseudoinversion or singular value filtering

	KinematicSettings.HowToInvertJacobian = HowToInvertJacobian;
	switch(HowToInvertJacobian)
	{
		case JP:
			break;
		case JD:
			{
				KinematicSettings.DampingFactorMaxSquared = FirstValue;
				KinematicSettings.SingularValueTreshold = SecondValue;
			}
			break;
		case SVF:
			{
				KinematicSettings.AssignedSmallestSingularValue = FirstValue;
				KinematicSettings.SVFshapeFactor = SecondValue;
			}
			break;
		case SVFmod:
			{
				KinematicSettings.AssignedSmallestSingularValue = FirstValue;
				KinematicSettings.SingularValueTreshold = SecondValue;
			}
			break;
		case JT:
			break;
	}
}

void KinematicModel::SetErrorGain( double ErrorGain )
{
	//~ Set the error gain for the CLIK method
	KinematicSettings.ErrorGain = ErrorGain;
}

void KinematicModel::SetErrorSaturation( double ErrorSaturation )
{
	//~ Set the saturation level of error for the CLIK method
	KinematicSettings.ErrorSaturation = ErrorSaturation;
}

void KinematicModel::SetIntegrationMethod( integrationType IntegrationMethod )
{
	//~ Set the integration method: EE -- explicit Euler, EM -- explicit midpoint, RK4 -- Runge-Kutta 4th order
	KinematicSettings.IntegrationMethod = IntegrationMethod;
}

integrationType KinematicModel::GetIntegrationMethod()
{
	//~ Get the type of the integration method
	return KinematicSettings.IntegrationMethod;
}	

void KinematicModel::SetSubtask( subtaskType Subtask )
{
	//~ Set the secondary task in the redundancy resolution.
	KinematicSettings.Subtask = Subtask;
}

subtaskType KinematicModel::GetSubtask()
{
	//~ Get the type of the secondary task in the redundancy resolution.
	return KinematicSettings.Subtask;
}	

void KinematicModel::SetSubtaskConstraintGain( double SubtaskConstraintGain )
{
	//~ Set the value of the subtask constraint gain (it scales the nullspace velocity)
	KinematicSettings.SubtaskConstraintGain = SubtaskConstraintGain;
}

double KinematicModel::GetSubtaskConstraintGain()
{
	//~ Get the value of the subtask constraint gain
	return KinematicSettings.SubtaskConstraintGain;
}

void KinematicModel::SetMultitaskType( multitaskType MultitaskType )
{
	//~ Set the type of multitask formulation
	KinematicSettings.MultitaskType = MultitaskType;
}

void KinematicModel::SetJointLimits( const Eigen::VectorXd &JointLimitMax, const Eigen::VectorXd &JointLimitMin )
{
	//~ Set the joint limits
	if (JointLimitMax.size() == DOF && JointLimitMin.size() == DOF)
	{
		KinematicSettings.JointLimitMax = JointLimitMax;
		KinematicSettings.JointLimitMin = JointLimitMin;
		KinematicSettings.JointMid = 0.5 * (KinematicSettings.JointLimitMax + KinematicSettings.JointLimitMin);
	}
}

void KinematicModel::SetObstaclePoints( const Eigen::MatrixXd ObstacleLocations )
{
	//~ Set the obstacles' locations as a 3xNumberOfObstacles matrix of 3x1 vectors (x,y,z)
	if (ObstacleLocations.rows() == 3)
	{
		if (KinematicSettings.ObstaclePoints.cols() != ObstacleLocations.cols())
		{
			KinematicSettings.ObstaclePoints.resize(3, ObstacleLocations.cols());
		}
		KinematicSettings.ObstaclePoints = ObstacleLocations;
	}
}

void KinematicModel::SetObstacleAvoidanceParameters( double ObstacleSphereOfInfluence, double ObstacleUnityGain, double ObstacleAvoidanceNominalVel )
{
	//~ Set the parameters of the obstacle avoidance subtask
	KinematicSettings.ObstacleSphereOfInfluence = ObstacleSphereOfInfluence;
	KinematicSettings.ObstacleUnityGain = ObstacleUnityGain;
	KinematicSettings.ObstacleAvoidanceNominalVel = ObstacleAvoidanceNominalVel;
}

} //namespace RedundantRobot
