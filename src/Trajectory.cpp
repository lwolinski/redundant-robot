//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "RedundantRobot/Trajectory.h"

namespace RedundantRobot
{
Trajectory::Trajectory( const Eigen::MatrixXd& Points, const Eigen::RowVectorXd& SegmentTimes, double AccelerationPeriod, double DecelerationPeriod, double BlendingPeriod )
: Points(Points), SegmentTimes(SegmentTimes), SegmentNumber(1), PathTime(SegmentTimes(1)), AccelerationPeriod(AccelerationPeriod), DecelerationPeriod(DecelerationPeriod), BlendingPeriod(BlendingPeriod)
{
	//~ TODO: if the number of columns of Points is not equal to the number of elements in PointTimes, then FAIL
}

Eigen::VectorXd Trajectory::GetTrajectory(double t)
{
	//~ Get the end effector position and velocity
	
	Eigen::VectorXd EndEffectorPosAndVel(6);
	
	if (BlendingPeriod > 0.0)
	{
		EndEffectorPosAndVel = GetBlendedTrajectory(t);
	}
	else
	{
		EndEffectorPosAndVel = GetStandardTrajectory(t);
	}
	
	return EndEffectorPosAndVel;
}

Eigen::VectorXd Trajectory::GetStandardTrajectory(double t)
{
	//~ The robot will stop at the end of the each linear segment.
	
	//~ Check whether it is time to increase the segment number (but only if it is not already the last segment)
	if( (t > PathTime) && (SegmentNumber < SegmentTimes.cols() - 1) )
	{
		++SegmentNumber;
		PathTime += SegmentTimes(SegmentNumber);
	}
	
	//~ Get the start and end positions for the current segment; get the direction unit vector and the segment length; get the times
	Eigen::Vector3d StartPos = Points.col(SegmentNumber - 1);
	Eigen::Vector3d EndPos = Points.col(SegmentNumber);
	double SegmentLength = (EndPos - StartPos).norm();
	Eigen::Vector3d Direction = (EndPos - StartPos) / SegmentLength;
	double SegmentStartTime = PathTime - SegmentTimes(SegmentNumber);
	double SegmentEndTime = SegmentTimes(SegmentNumber);
	double LocalTime = t - SegmentStartTime;
	
	//~ Compute the end effector position and velocity for a given time
	//~ Eigen::Vector2d PathDisplacementAndVel = ParabolicVelProfile(SegmentLength, LocalTime, SegmentEndTime);
	Eigen::Vector2d PathDisplacementAndVel = SinusoidalVelProfile(SegmentLength, LocalTime, SegmentEndTime);
	Eigen::VectorXd EndEffectorPosAndVel(6);
	EndEffectorPosAndVel.head(3) = StartPos + Direction * PathDisplacementAndVel(0);
	EndEffectorPosAndVel.tail(3) = Direction * PathDisplacementAndVel(1);
	
	return EndEffectorPosAndVel;
}

Eigen::VectorXd Trajectory::GetBlendedTrajectory(double t)
{
	//~ The robot will not stop at the end of the each linear segment -- the segments will overlap each other.
	
	//~ Check whether it is time to increase the segment number (but only if it is not already the last segment)
	if( (t > PathTime) && (SegmentNumber < SegmentTimes.cols() - 1) )
	{
		++SegmentNumber;
		PathTime += (1.0 - BlendingPeriod) * SegmentTimes(SegmentNumber);
	}
	
	//~ Get the start and end positions for the current segment; get the direction unit vector and the segment length; get the times
	Eigen::Vector3d StartPos = Points.col(SegmentNumber - 1);
	Eigen::Vector3d EndPos = Points.col(SegmentNumber);
	double SegmentLength = (EndPos - StartPos).norm();
	Eigen::Vector3d Direction = (EndPos - StartPos) / SegmentLength;
	double SegmentStartTime = PathTime - SegmentTimes(SegmentNumber);
	double SegmentEndTime = SegmentTimes(SegmentNumber);
	double LocalTime = t - SegmentStartTime;
	
	//~ Compute the end effector position and velocity for a given time
	Eigen::Vector2d PathDisplacementAndVel = SinusoidalVelProfile(SegmentLength, LocalTime, SegmentEndTime);
	Eigen::VectorXd EndEffectorPosAndVel(6);
	EndEffectorPosAndVel.head(3) = StartPos + Direction * PathDisplacementAndVel(0);
	EndEffectorPosAndVel.tail(3) = Direction * PathDisplacementAndVel(1);
	
	if (SegmentNumber < SegmentTimes.cols() - 1) // there is no segment after the last to blend with
	{
		if ( LocalTime > SegmentEndTime - BlendingPeriod * SegmentTimes(SegmentNumber + 1) )
		{
			//~ Time to start the next segment
			int NextSegmentNumber = SegmentNumber + 1;
			
			Eigen::Vector3d NextStartPos = Points.col(NextSegmentNumber - 1);
			Eigen::Vector3d NextEndPos = Points.col(NextSegmentNumber);
			double NextSegmentLength = (NextEndPos - NextStartPos).norm();
			Eigen::Vector3d NextDirection = (NextEndPos - NextStartPos) / NextSegmentLength;
			double NextSegmentStartTime = PathTime;
			double NextSegmentEndTime = SegmentTimes(NextSegmentNumber);
			double NextLocalTime = LocalTime - ( SegmentEndTime - BlendingPeriod * NextSegmentEndTime );
			
			//~ Compute the end effector position and velocity for a given time
			Eigen::Vector2d NextPathDisplacementAndVel = SinusoidalVelProfile(NextSegmentLength, NextLocalTime, NextSegmentEndTime);
			Eigen::VectorXd NextEndEffectorPosAndVel(6);
			NextEndEffectorPosAndVel.head(3) = NextDirection * NextPathDisplacementAndVel(0);
			NextEndEffectorPosAndVel.tail(3) = NextDirection * NextPathDisplacementAndVel(1);
			
			EndEffectorPosAndVel += NextEndEffectorPosAndVel;
		}
	
	}
	
	return EndEffectorPosAndVel;
}

Eigen::Vector2d Trajectory::ParabolicVelProfile( double SegmentLength, double LocalTime, double SegmentEndTime )
{
	//~ Get the displacement along the path and the path speed when the velocity profile is parabolic
	Eigen::Vector2d PathDisplacementAndVel;
	
	PathDisplacementAndVel(0) = SegmentLength * ( 3.0 * (LocalTime / SegmentEndTime) * (LocalTime / SegmentEndTime) - 2.0 * (LocalTime / SegmentEndTime) * (LocalTime / SegmentEndTime) * (LocalTime / SegmentEndTime) );
	
	PathDisplacementAndVel(1) = 6.0 * SegmentLength * ( LocalTime / (SegmentEndTime * SegmentEndTime) - (LocalTime * LocalTime) / (SegmentEndTime * SegmentEndTime * SegmentEndTime) );
	
	return PathDisplacementAndVel;
}

Eigen::Vector2d Trajectory::SinusoidalVelProfile( double SegmentLength, double LocalTime, double SegmentEndTime )
{
	//~ Get the displacement along the path and the path speed when the velocity profile is "sinusoidal"
	Eigen::Vector2d PathDisplacementAndVel;
	
	double MaxVel = 2.0 * SegmentLength / ( (2.0 - AccelerationPeriod - DecelerationPeriod) * SegmentEndTime );
	
	if (LocalTime < AccelerationPeriod * SegmentEndTime)
	{
		double x = M_PI / (AccelerationPeriod * SegmentEndTime) * LocalTime;
		PathDisplacementAndVel(0) = MaxVel / ( 4.0 * M_PI * M_PI / (AccelerationPeriod * SegmentEndTime) ) * ( 2.0 * x * x + cos(2.0 * x) ) - (MaxVel * AccelerationPeriod * SegmentEndTime) / (4.0 * M_PI * M_PI);
		PathDisplacementAndVel(1) = (x - sin(x) * cos(x)) / M_PI * MaxVel;
	}
	else if (LocalTime > SegmentEndTime) // this condition can be reached on the last segment while using the multistep integration methods in the IK solver
	{
		PathDisplacementAndVel(0) = SegmentLength;
		PathDisplacementAndVel(1) = 0.0;
	}
	else if (LocalTime > (1.0 - DecelerationPeriod) * SegmentEndTime)
	{
		double PathSoFar = MaxVel / ( 4.0 * M_PI * M_PI / (AccelerationPeriod * SegmentEndTime) ) * ( 2.0 * M_PI * M_PI + 1.0 ) - (MaxVel * AccelerationPeriod * SegmentEndTime) / (4.0 * M_PI * M_PI) + MaxVel * SegmentEndTime * ( 1.0 - (AccelerationPeriod + DecelerationPeriod) );
		double x = -M_PI / (DecelerationPeriod * SegmentEndTime) * ( LocalTime - SegmentEndTime * (1.0 - DecelerationPeriod) );
		double a1 = -M_PI / (DecelerationPeriod * SegmentEndTime);
		double a2 = SegmentEndTime * (1.0 - DecelerationPeriod);
		PathDisplacementAndVel(0) = MaxVel * ( 2.0 * a1 * LocalTime * (a1 * (LocalTime - 2.0 * a2) / M_PI + 2.0) + cos( 2.0 * a1 * (a2 - LocalTime) ) / M_PI ) / (4.0 * a1) - ( MaxVel * ( 2.0 * a1 * a2 * (2.0 - a1 * a2 / M_PI) + (1 / M_PI) ) ) / (4.0 * a1) + PathSoFar;
		PathDisplacementAndVel(1) = ( (x - sin(x) * cos(x)) / M_PI + 1.0 ) * MaxVel;
	}
	else
	{
		double PathSoFar = MaxVel / ( 4.0 * M_PI * M_PI / (AccelerationPeriod * SegmentEndTime) ) * ( 2.0 * M_PI * M_PI + 1.0 ) - (MaxVel * AccelerationPeriod * SegmentEndTime) / (4.0 * M_PI * M_PI);
		PathDisplacementAndVel(0) = MaxVel * (LocalTime - AccelerationPeriod * SegmentEndTime) + PathSoFar;
		PathDisplacementAndVel(1) = MaxVel;
	}
	
	return PathDisplacementAndVel;
}

//~ integrate (A1 * t - sin(A1 * t) * cos(A1 * t)) * A2
//~ (A2 (2 A1^2 t^2 + cos(2 A1 t)))/(4 A1) + constant

//~ integrate ( A4 * (A1 * (t - A2) - sin(A1 * (t - A2)) * cos(A1 * (t - A2)) ) + A3 ) * A5
//~ (A5 (-2 A1^2 A4 t (2 A2 - t) + A4 cos(2 A1 A2 - 2 A1 t) + 4 A1 A3 t))/(4 A1) + constant
	
} //namespace RedundantRobot
